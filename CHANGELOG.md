﻿# CHANGELOG

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/).
  
## [v1.0.6] - 2019-03-25  
  
### Changed  
* Fix en error de nomenclatura  

## [v1.0.5] - 2019-02-12  
  
### Changed  
* Mejorada trazabilidad de errores en PredictManager  
  
## [v1.0.4] - 2018-10-25  
  
### Changed  
* Añadidos parámetros reqId a las funciones principales del módulo  
  
## [v1.0.3] - 2018-08-03  
  
### Changed  
* Añadidas comprobaciones de parámetros  

## [v1.0.2] - 2018-07-24

### Changed
* Añadido domain en los logs
* Mejora en el tratamiento de errores
* Pasado FakeLog a componentes
* Fusionados TrainManager y PatchManager, ahora no existe PatchManager y todas sus operaciones están en TrainManager
* Simplificada inyección de dependencias
* Ensembles en PredictManager ahora es opcional
* El módulo ahora devuelve el estado de la tarea de reentrenamiento y de parcheo
* Reimplementado FakeStorage para funcionar con querys
* Añadimos tests funcionales faltantes en TrainManager
* Implementados tests de entrenamiento con nuevos servicios web de Azure ML
* Añadidos tests de componentes y ensembles

## [v1.0.1] - 2018-07-06

### Changed
* Añadidas llamas síncronas en Train Manager

## [v1.0.0] - 2018-07-05

### Changed
* Primera versión completa del módulo, incluyendo test funcionales y de integración

## [v0.0.2] - 2018-07-02

### Changed
* Orientación a inyección de dependencias

## [v0.0.1] - 2018-06-18

### Changed
* Versión alpha