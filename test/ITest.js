const config = require('./config.json');
const DIC = require('./DIC.js');

const ITest = class ITest {

  constructor() { }

  before({ done }) { done(); }
  after({ done }) { done(); }
  do({ done }) { done(); }

  getDIC({ }) {
    return new DIC({
      storage: config.storage,
      webServices: config.webServices
    });
  }

}

module.exports = ITest;