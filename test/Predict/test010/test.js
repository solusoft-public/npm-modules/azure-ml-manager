const expect = require("chai").expect;

const ITest = require('../../ITest');
const FakeSDK = require('../../common-fakes/FakeSDK');
const AzureMLWebServiceEnsemble = require('../../../libs/ml/prediction/ensemble/implementations/AzureMLWebService');
const PredictManager = require('../../../libs/ml/prediction/MLPredictManager');

//properties
const _dic = Symbol("dic");
const _sdks = Symbol("sdks");
const _ensembles = Symbol("ensembles");

//methods
const _getSdks = Symbol("getSdks");
const _getEnsembles = Symbol("getEnsembles");

const test = class test extends ITest {

  constructor() {
    super();          
    this[_dic] = super.getDIC({ });
    this[_sdks] = this[_getSdks]({ });
    this[_ensembles] = this[_getEnsembles]({ sdks: this[_sdks] });
  }

  [_getSdks]({ }) {
    const webServices = this[_dic].getWebServices();
    const log = this[_dic].getLog();
    const sexSLPrediction = require('./predictions/sex_sl.json');
    const sdksPrediction = {
      "sex_sl": sexSLPrediction,
    };
    const sdkNames = Object.keys(sdksPrediction);
    return Object.keys(webServices)
      .filter(key => sdkNames.includes(key))
      .map(webServiceName => {
        const webService = webServices[webServiceName];
        const { outputPredictName: outputName } = webService;
        const prediction = sdksPrediction[webServiceName];
        const predictionObject = {
          "Results": {
            [outputName]: [
              prediction
            ]
          }          
        };
        const sdk = new FakeSDK({ webService: webService, prediction: predictionObject, log: log });
        return {
          webServiceName,
          sdk
        };
      })
      .reduce((obj, curr) => {
        const { webServiceName, sdk } = curr;
        obj[webServiceName] = sdk;
        return obj;
      }, {});
  }

  [_getEnsembles]({ sdks }) {
    const webServices = this[_dic].getWebServices();
    const log = this[_dic].getLog();    
    const ensemblesConfig = {      
      "sex_sl": {
        "name": "C_SEX_SL",
        "inputColumns": ["label1", "label2"],
        "outputColumns": [null, null, null, null, null, null, null, null, null, "Scored Labels", null, null, null, null, null],    
      }
    };
    const ensemblesNames = Object.keys(ensemblesConfig);
    const azureMLWSEnsembles = Object.keys(webServices)
    .filter(key => ensemblesNames.includes(key))
    .map(webServiceName => {
      const webService = webServices[webServiceName];
      const { inputName, outputPredictName: outputName } = webService;
      const { name, inputColumns, outputColumns } = ensemblesConfig[webServiceName];
      const sdk = sdks[webServiceName];
      return new AzureMLWebServiceEnsemble({ 
        name: name,
        outputColumns: outputColumns,
        inputName: inputName,
        outputName: outputName,
        inputColumns: inputColumns,
        azureMLSDK: sdk,
        log: log,
      });
    });

    const ensembles = new Array(15);
    ensembles.fill(null);
    ensembles[9] = azureMLWSEnsembles[0];
    return ensembles;
  }

  do({ done }) {
    const input = require('./input.json');
    const predictManager = new PredictManager({ ensembles: this[_ensembles], log: this[_dic].getLog() });
    predictManager.disambiguateVectors({ 
      componentOutputsVector: input,
      callback: (err, results) => {
        //assert
        expect(err).to.not.be.ok; //no debe dar error
        const output = require('./output.json');
        expect(results).to.deep.equals(output);
        done();
      }
    });
  }

}

module.exports = test;