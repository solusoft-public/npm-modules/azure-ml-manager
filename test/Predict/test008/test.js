const expect = require("chai").expect;
const assert = require('assert');

const ITest = require('../../ITest');

const test = class test extends ITest {

  constructor({}) {
    super();
  }

  do({ done }) {
    const dic = super.getDIC({});
    const AzureWebServiceEnsemble = require('../../../libs/ml/prediction/ensemble/implementations/AzureMLWebService');    
    const prediction = require('./prediction.json');
    const SDK = require('../../common-fakes/FakeSDK');
    const sdk = new SDK({ webService: {}, prediction: prediction, log: dic.getLog() });
    const azureWebServiceEnsemble = new AzureWebServiceEnsemble({ 
      name: "AzureWebService",
      outputColumns: ["lblpp"],
      inputName: "input1",
      outputName: "output1",
      inputColumns: 1231,
      azureMLSDK: sdk
    });   
    const input = require('./input.json');
    azureWebServiceEnsemble.disambiguateVector({
      input: input,
      callback: (err, result) => {
        expect(err).to.be.ok; //debe dar error
        done();
      }
    });
  }

}

module.exports = test;