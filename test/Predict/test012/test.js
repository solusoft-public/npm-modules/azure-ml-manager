const expect = require("chai").expect;
const AzureStorage = require('azure-storage');

const ITest = require('../../ITest');
const FakeSDK = require('../../common-fakes/FakeSDK');
const TableQuery = AzureStorage.TableQuery;
const AzureDataTable = require('../../../libs/ml/prediction/component/implementations/AzureDataTable');
const AzureMLWebService = require('../../../libs/ml/prediction/component/implementations/AzureMLWebService');
const AzureMLWebServiceEnsemble = require('../../../libs/ml/prediction/ensemble/implementations/AzureMLWebService');
const PredictManager = require('../../../libs/ml/prediction/MLPredictManager');

//properties
const _dic = Symbol("dic");
const _pid = Symbol("pid");
const _uid = Symbol("uid");
const _sdks = Symbol("sdks");
const _components = Symbol("components");
const _ensembles = Symbol("ensembles");

//methods
const _getSdks = Symbol("getSdks");
const _getComponents = Symbol("getComponents");
const _getEnsembles = Symbol("getEnsembles");

const test = class test extends ITest {

  constructor() {
    super();
    this[_dic] = super.getDIC({ });
    this[_pid] = "etur";
    this[_uid] = "00014eb8-d7c9-4703-8753-912bbc2d3172";
    this[_sdks] = this[_getSdks]({ });
    this[_components] = this[_getComponents]({ sdks: this[_sdks] });
    this[_ensembles] = this[_getEnsembles]({ sdks: this[_sdks] });
  }

  [_getSdks]({ }) {
    const webServices = this[_dic].getWebServices();
    const log = this[_dic].getLog();
    const sexSVNPrediction = require('./predictions/sex_svm.json');
    const workClassPrediction = require('./predictions/workClass_df.json');
    const maritalStatusPrediction = require('./predictions/maritalStatus_df.json');
    const sexSLPrediction = require('./predictions/sex_sl.json');
    const sdksPrediction = {
      "sex_svm": sexSVNPrediction,
      "workClass_df": workClassPrediction,
      "maritalStatus_df": maritalStatusPrediction,
      "sex_sl": sexSLPrediction,
    };
    const sdkNames = Object.keys(sdksPrediction);
    return Object.keys(webServices)
      .filter(key => sdkNames.includes(key))
      .map(webServiceName => {
        const webService = webServices[webServiceName];
        const { outputPredictName: outputName } = webService;
        const prediction = sdksPrediction[webServiceName];
        const predictionObject = {
          "Results": {
            [outputName]: [
              prediction
            ]
          }          
        };
        const sdk = new FakeSDK({ webService: webService, prediction: predictionObject, log: log });
        return {
          webServiceName,
          sdk
        };
      })
      .reduce((obj, curr) => {
        const { webServiceName, sdk } = curr;
        obj[webServiceName] = sdk;
        return obj;
      }, {});
  }

  [_getComponents]({ sdks }) {
    const webServices = this[_dic].getWebServices();
    const storageManager = this[_dic].getStorageManager();
    const log = this[_dic].getLog();
    const componentsConfig = {
      "sex_svm": {
        "name": "C_GEN_SVM",
        "inputColumns": ["age", "workClass", "fnlwgt", "education", null, "maritalStatus", null, null, "race", null, null, null, null, null, "income"],
        "outputColumns": 123,
      },
      "workClass_df": {
        "name": "C_WORKCLASS",
        "inputColumns": new Date(),
        "outputColumns": [null, "Scored Labels", null, null, null, null, null, null, null, null, null, null, null, null, null],
      },
      "maritalStatus_df": {
        "name": "C_MARITAL_STATUS",
        "inputColumns": ["age", "workClass", "fnlwgt", "education", null, null, null, null, "race", "sex", null, null, null, null, "income"],
        "outputColumns": "estofalla",
      }
    };
    const componentNames = Object.keys(componentsConfig);
    let components = [];
    const query = new TableQuery()
      .where('PartitionKey eq ? and RowKey eq ?', this[_pid], this[_uid]);
    components[0] = new AzureDataTable({ 
      name: "C_IMG",        
      outputColumns: ["age", null, null, null, null, null, null, null, null, "sex", null, null, null, null, null],
      query: {},
      storageManager: storageManager,
      tableName: "componentOutputs",
      log: log,
    });   
    const azureMLWSComponents = Object.keys(webServices)
      .filter(key => componentNames.includes(key))
      .map(webServiceName => {
        const webService = webServices[webServiceName];
        const { inputName, outputPredictName: outputName } = webService;
        const { name, inputColumns, outputColumns } = componentsConfig[webServiceName];
        const sdk = sdks[webServiceName];
        return new AzureMLWebService({ 
          name: name,
          outputColumns: outputColumns,
          inputName: inputName,
          outputName: outputName,
          inputColumns: inputColumns,
          azureMLSDK: sdk,
          log: log,
        });
      });
    components = components.concat(azureMLWSComponents);
    return components;
  }

  [_getEnsembles]({ sdks }) {
    const webServices = this[_dic].getWebServices();
    const log = this[_dic].getLog();    
    const ensemblesConfig = {      
      "sex_sl": {
        "name": "C_SEX_SL",
        "inputColumns": ["label1", "label2"],
        "outputColumns": 0.0,    
      }
    };
    const ensemblesNames = Object.keys(ensemblesConfig);
    const azureMLWSEnsembles = Object.keys(webServices)
    .filter(key => ensemblesNames.includes(key))
    .map(webServiceName => {
      const webService = webServices[webServiceName];
      const { inputName, outputPredictName: outputName } = webService;
      const { name, inputColumns, outputColumns } = ensemblesConfig[webServiceName];
      const sdk = sdks[webServiceName];
      return new AzureMLWebServiceEnsemble({ 
        name: name,
        outputColumns: outputColumns,
        inputName: inputName,
        outputName: outputName,
        inputColumns: inputColumns,
        azureMLSDK: sdk,
        log: log,
      });
    });

    const ensembles = new Array(15);
    ensembles.fill(null);
    ensembles[9] = azureMLWSEnsembles[0];
    return ensembles;
  }

  before({ done }) {
    const storageManager = this[_dic].getStorageManager();
    const entity = require('./predictions/watson.json');
    entity.PartitionKey = this[_pid];
    entity.RowKey = this[_uid];
    entity.identifier = "C_IMG";
    const entities = [entity];    
    storageManager.uploadEntitiesToTable({ entities: entities, tableName: "componentOutputs", callback: done });
  }


  do({ done }) {
    const inputJSON = require('./input.json');
    const { input } = inputJSON;
    const predictManager = new PredictManager({ components: this[_components], ensembles: this[_ensembles], log: this[_dic].getLog() });
    predictManager.doPredictionFlow({ 
      input: input,
      callback: (err, results) => {
        //assert
        expect(err).to.not.be.ok; //no debe dar error
        //No obstante debe devolver un vector de salida con todas las posiciones nulas
        const output = require('./output.json');
        expect(results).to.deep.equals(output);
        done();
      }
    });
  }

}

module.exports = test;