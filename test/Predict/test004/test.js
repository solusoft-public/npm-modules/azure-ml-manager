const expect = require("chai").expect;
const assert = require('assert');

const ITest = require('../../ITest');

const test = class test extends ITest {

  constructor({}) {
    super();
  }

  do({ done }) {
    const dic = super.getDIC({});
    const AzureWebServiceComponent = require('../../../libs/ml/prediction/component/implementations/AzureMLWebService');    
    const prediction = require('./prediction.json');
    const SDK = require('../../common-fakes/FakeSDK');
    const sdk = new SDK({ webService: {}, prediction: prediction, log: dic.getLog() });
    const azureWebServiceComponent = new AzureWebServiceComponent({ 
      name: "AzureWebService",
      outputColumns: ["lblpp"],
      inputName: "input1",
      outputName: "output1",
      inputColumns: ["device_price", "app_spending"],
      azureMLSDK: sdk
    });   
    const input = require('./input.json');
    azureWebServiceComponent.getComponentOutput({
      input: input,
      callback: (err, result) => {
        //El mock del SDK de Azure devuelve la predicción, especificada en el archivo prediction.json
        const output = require('./output.json');
        expect(err).to.not.be.ok; //no debe dar error
        expect(result).to.deep.equal(output);
        done();
      }
    });
  }

}

module.exports = test;