const expect = require("chai").expect;
const assert = require('assert');
const ObjectManager = require('../../../libs/ObjectManager');

const ITest = require('../../ITest');

//properties
const _dic = Symbol("dic");
const _pid = Symbol("pid");
const _uid = Symbol("uid");

const test = class test extends ITest {

  constructor({}) {
    super();
    this[_dic] = super.getDIC({});
    this[_pid] = "mypid";
    this[_uid] = "myuid";
  }

  before({ done }) {    
    const keys = ["gender", "age"];
    const values = ["female", 42];
    const objectManager = new ObjectManager({ log: this[_dic].getLog() });
    const entity = objectManager.buildObject({ keys: keys, values: values });
    entity.identifier = "C_IMG";
    entity.PartitionKey = this[_pid];
    entity.RowKey = this[_uid];
    const storageManager = this[_dic].getStorageManager();
    storageManager.uploadEntitiesToTable({
      entities: [entity],
      tableName: 'testTable',
      callback: done
    });
  }

  do({ done }) {        
    const AzureDataTableComponent = require('../../../libs/ml/prediction/component/implementations/AzureDataTable');
    const AzureStorage = require('azure-storage');
    const TableQuery = AzureStorage.TableQuery;
    const storageManager = this[_dic].getStorageManager();    
    const query = new TableQuery().where('PartitionKey eq ? and RowKey eq ?', this[_pid], this[_uid]);         
    const azureTableComponent = new AzureDataTableComponent({ 
      name: "AzureTable",        
      outputColumns: ["gender", "age"],
      storageManager: storageManager,
      tableName: "testTable",
      query: query,
    }); 
    const input = require('./input.json');
    azureTableComponent.getComponentOutput({
      input: input,
      callback: (err, result) => {
        //El componente de AzureTable consulta una tabla de Azure de acuerdo a una query especificada
        const output = require('./output.json');
        expect(err).to.not.be.ok; //no debe dar error
        expect(result).to.deep.equal(output);
        done();
      }
    });
  }

}

module.exports = test;