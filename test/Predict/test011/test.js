const expect = require("chai").expect;

const ITest = require('../../ITest');
const FakeSDK = require('../../common-fakes/FakeSDK');
const AzureMLWebServiceEnsemble = require('../../../libs/ml/prediction/ensemble/implementations/AzureMLWebService');
const PredictManager = require('../../../libs/ml/prediction/MLPredictManager');

//properties
const _dic = Symbol("dic");

const test = class test extends ITest {

  constructor() {
    super();          
    this[_dic] = super.getDIC({ });
  }

  do({ done }) {
    const input = require('./input.json');
    const predictManager = new PredictManager({ log: this[_dic].getLog() });
    predictManager.reduceVectors({ 
      disambiguatedVector: input,
      callback: (err, results) => {
        //assert
        expect(err).to.not.be.ok; //no debe dar error
        const output = require('./output.json');
        expect(results).to.deep.equals(output);
        done();
      }
    });
  }

}

module.exports = test;