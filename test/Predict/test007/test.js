const expect = require("chai").expect;
const assert = require('assert');

const ITest = require('../../ITest');

const test = class test extends ITest {

  constructor({}) {
    super();
  }

  do({ done }) {
    const dic = super.getDIC({});
    const AzureWebServiceComponent = require('../../../libs/ml/prediction/component/implementations/AzureMLWebService');
    const azureWebServiceComponent = new AzureWebServiceComponent({ 
      name: "AzureWebService",
      outputColumns: ["lblpp"],
      inputName: "input1",
      outputName: "output1",
      inputColumns: ["device_price", "app_spending"],
      azureMLSDK: {}
    });   
    const input = require('./input.json');
    azureWebServiceComponent.getComponentOutput({
      input: input,
      callback: (err, result) => {
        expect(err).to.be.ok; //no debe dar error
        done();
      }
    });
  }

}

module.exports = test;