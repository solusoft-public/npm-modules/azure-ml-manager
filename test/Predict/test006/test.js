const expect = require("chai").expect;
const assert = require('assert');
const ObjectManager = require('../../../libs/ObjectManager');

const ITest = require('../../ITest');

//properties
const _dic = Symbol("dic");
const _pid = Symbol("pid");
const _uid = Symbol("uid");

const test = class test extends ITest {

  constructor({}) {
    super();
    this[_dic] = super.getDIC({});
    this[_pid] = "mypid";
    this[_uid] = "myuid";
  }

  do({ done }) {        
    const AzureDataTableComponent = require('../../../libs/ml/prediction/component/implementations/AzureDataTable');
    const AzureStorage = require('azure-storage');
    const TableQuery = AzureStorage.TableQuery;
    const storageManager = this[_dic].getStorageManager();        
    const azureTableComponent = new AzureDataTableComponent({ 
      name: "AzureTable",        
      outputColumns: ["gender", 454],
      storageManager: storageManager,
      tableName: "testTable",
      query: {},
    }); 
    const input = require('./input.json');
    azureTableComponent.getComponentOutput({
      input: input,
      callback: (err, result) => {
        expect(err).to.be.ok; //debe dar error
        done();
      }
    });
  }

}

module.exports = test;