const expect = require("chai").expect;
const Async = require('async');

const ITest = require('../../ITest');
const FakeSDK = require('../../common-fakes/FakeSDK');
const TrainManager = require('../../../libs/ml/train/MLTrainBESManager');

//properties
const _dic = Symbol("dic");
const _sdks = Symbol("sdks");
const _patchList = Symbol("patchList");
const _trainResults = Symbol("trainResults");
const _trainManager = Symbol("trainManager");

//methods
const _getSdks = Symbol("getSdks");
const _getPatchList = Symbol("getPatchList");

const test = class test extends ITest {

  constructor() {    
    super();
    this[_dic] = super.getDIC({ });
    this[_sdks] = this[_getSdks]({ });
    this[_patchList] = this[_getPatchList]({ sdks: this[_sdks] });    
  }

  [_getSdks]({ }) {
    const webServices = this[_dic].getWebServices();
    const log = this[_dic].getLog();
    const sdkNames = [
      "training",
      "sex_svm",
      "sex_sl",
      "workClass_df",
      "maritalStatus_df"
    ]; 
    return Object.keys(webServices)
      .filter(key => sdkNames.includes(key))
      .map(webServiceName => {
        const webService = webServices[webServiceName];
        const sdk = new FakeSDK({ webService: webService, log: log });
        return {
          webServiceName,
          sdk
        };
      })
      .reduce((obj, curr) => {
        const { webServiceName, sdk } = curr;
        obj[webServiceName] = sdk;
        return obj;
      }, {});
  }

  [_getPatchList]({ sdks }) {
    const webServices = this[_dic].getWebServices();
    const patchNames = [
      "sex_svm",
      "sex_sl",
      "workClass_df",
      "maritalStatus_df"
    ]; 
    return Object.keys(webServices)
      .filter(key => patchNames.includes(key))
      .map(webServiceName => {
        const webService = webServices[webServiceName];
        const { resourceName, outputTrainingName: outputName } = webService;
        const sdk = sdks[webServiceName];
        return {
          sdk,
          resourceName,
          outputName
        };
      });
  }

  before({ done }) {
    const inputJSON = require('./input.json');
    const { inputs, outputs } = inputJSON;
    const storageManager = this[_dic].getStorageManager();
    const { training } = this[_sdks];
    const log = this[_dic].getLog();
    this[_trainManager] = new TrainManager({ storageManager: storageManager, azureMLSDK: training, log: log });
    this[_trainManager].retrainModelSync({
      containerName: "pmtrainoutputs",
      inputs: inputs,
      outputs: outputs,
      timeStep: 1000,
      callback: (err, results) => {
        //assert
        expect(err).to.not.be.ok; //no debe dar error
        this[_trainResults] = results;
        done();
      }
    });
  }

  do({ done }) {
    Async.series(
      [
        callback => {
          this[_trainManager].getJobStatus({ 
            jobId: this[_trainResults].jobId, 
            callback: (err, result) => {
              expect(err).to.not.be.ok; //no debe dar error
              const { training, patch } = result;
              expect(training).to.have.property("StatusCode", "Finished");
              expect(patch).to.have.property("StatusCode", "NotStarted");
              callback(err, result);
            }});
        },
        callback => {
          this[_trainManager].patchModelWithJobId({
            patchList: this[_patchList],
            jobId: this[_trainResults].jobId,
            callback: callback
          });
        },
        callback => {
          this[_trainManager].getJobStatus({ 
            jobId: this[_trainResults].jobId, 
            callback: (err, result) => {
              expect(err).to.not.be.ok; //no debe dar error
              const { training, patch } = result;
              expect(training).to.have.property("StatusCode", "Finished");
              expect(patch).to.have.property("StatusCode", "Finished");
              callback(err, result);
            }});
        }
      ],
      (err, results) => {
        //assert
        expect(err).to.not.be.ok; //no debe dar error
        done();
      }
    );
  }

}

module.exports = test;