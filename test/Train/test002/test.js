const expect = require("chai").expect;

const ITest = require('../../ITest');
const FakeSDK = require('../../common-fakes/FakeSDK');
const TrainManager = require('../../../libs/ml/train/MLTrainBESManager');

//properties
const _dic = Symbol("dic");
const _sdks = Symbol("sdks");
const _patchList = Symbol("patchList");

//methods
const _getSdks = Symbol("getSdks");
const _getPatchList = Symbol("getPatchList");

const test = class test extends ITest {

  constructor() {    
    super();
    this[_dic] = super.getDIC({ });
    this[_sdks] = this[_getSdks]({ });
    this[_patchList] = this[_getPatchList]({ sdks: this[_sdks] });    
  }

  [_getSdks]({ }) {
    const webServices = this[_dic].getWebServices();
    const log = this[_dic].getLog();
    const sdkNames = [
      "training",
      "sex_svm",
      "sex_sl",
      "workClass_df",
      "maritalStatus_df"
    ]; 
    return Object.keys(webServices)
      .filter(key => sdkNames.includes(key))
      .map(webServiceName => {
        const webService = webServices[webServiceName];
        const sdk = new FakeSDK({ webService: webService, log: log });
        return {
          webServiceName,
          sdk
        };
      })
      .reduce((obj, curr) => {
        const { webServiceName, sdk } = curr;
        obj[webServiceName] = sdk;
        return obj;
      }, {});
  }

  [_getPatchList]({ sdks }) {
    const webServices = this[_dic].getWebServices();
    const patchNames = [
      "sex_svm",
      "sex_sl",
      "workClass_df",
      "maritalStatus_df"
    ]; 
    return Object.keys(webServices)
      .filter(key => patchNames.includes(key))
      .map(webServiceName => {
        const webService = webServices[webServiceName];
        const { resourceName, outputTrainingName: outputName } = webService;
        const sdk = sdks[webServiceName];
        return {
          sdk,
          resourceName,
          outputName
        };
      });
  }

  do({ done }) {
    const inputJSON = require('./input.json');
    const { inputs, outputs } = inputJSON;
    const storageManager = this[_dic].getStorageManager();
    const { training } = this[_sdks];
    const log = this[_dic].getLog();
    const patchList = this[_patchList];
    const trainManager = new TrainManager({ storageManager: storageManager, azureMLSDK: training, log: log });
    trainManager.retrainModelAndPatch({
      containerName: "pmtrainoutputs",
      inputs: inputs,
      outputs: outputs,
      patchList: patchList,
      timeStep: 1000,
      callback: (err, results) => {
        //assert
        expect(err).to.not.be.ok; //no debe dar error
        done();
      }
    });
  }

}

module.exports = test;