const expect = require("chai").expect;

const ITest = require('../../ITest');
const FakeSDK = require('../../common-fakes/FakeSDK');
const TrainManager = require('../../../libs/ml/train/MLTrainBESManager');

//properties
const _dic = Symbol("dic");
const _sdks = Symbol("sdks");

//methods
const _getSdks = Symbol("getSdks");

const test = class test extends ITest {

  constructor() {
    super();
    this[_dic] = super.getDIC({ });
    this[_sdks] = this[_getSdks]({ });
  }
  
  [_getSdks]({ }) {
    const webServices = this[_dic].getWebServices();
    const log = this[_dic].getLog();
    return Object.keys(webServices)
      .filter(sdk => sdk == "training")
      .map(webServiceName => {
        const webService = webServices[webServiceName];
        const sdk = new FakeSDK({ webService: webService, log: log });
        return {
          webServiceName,
          sdk
        };
      })
      .reduce((obj, curr) => {
        const { webServiceName, sdk } = curr;
        obj[webServiceName] = sdk;
        return obj;
      }, {});
  }

  do({ done }) {
    const inputJSON = require('./input.json');
    const { inputs, outputs } = inputJSON;
    const storageManager = this[_dic].getStorageManager();
    const { training } = this[_sdks];
    const log = this[_dic].getLog();
    const trainManager = new TrainManager({ storageManager: storageManager, azureMLSDK: training, log: log });
    trainManager.retrainModelSync({
      containerName: "pmtrainoutputs",
      inputs: inputs,
      outputs: outputs,
      timeStep: 1000,
      callback: (err, results) => {
        //assert
        expect(err).to.not.be.ok; //no debe dar error
        done();
      }
    });
  }

}

module.exports = test;