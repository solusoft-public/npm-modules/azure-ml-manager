const expect = require("chai").expect;
const Async = require('async');

const ITest = require('../../ITest');
const FakeSDK = require('../../common-fakes/FakeSDK');
const TrainManager = require('../../../libs/ml/train/MLTrainBESManager');

//properties
const _dic = Symbol("dic");
const _sdks = Symbol("sdks");
const _jobId = Symbol("jobId");
const _trainManager = Symbol("trainManager");

//methods
const _getSdks = Symbol("getSdks");
const _getPatchList = Symbol("getPatchList");

const test = class test extends ITest {

  constructor() {    
    super();
    this[_dic] = super.getDIC({ });
    this[_sdks] = this[_getSdks]({ }); 
  }

  [_getSdks]({ }) {
    const webServices = this[_dic].getWebServices();
    const log = this[_dic].getLog();
    const sdkNames = [
      "training",
      "sex_svm",
      "sex_sl",
      "workClass_df",
      "maritalStatus_df"
    ]; 
    return Object.keys(webServices)
      .filter(key => sdkNames.includes(key))
      .map(webServiceName => {
        const webService = webServices[webServiceName];
        const sdk = new FakeSDK({ webService: webService, log: log });
        return {
          webServiceName,
          sdk
        };
      })
      .reduce((obj, curr) => {
        const { webServiceName, sdk } = curr;
        obj[webServiceName] = sdk;
        return obj;
      }, {});
  }

  before({ done }) {
    const inputJSON = require('./input.json');
    const { inputs, outputs } = inputJSON;
    const storageManager = this[_dic].getStorageManager();
    const { training } = this[_sdks];
    const log = this[_dic].getLog();
    this[_trainManager] = new TrainManager({ storageManager: storageManager, azureMLSDK: training, log: log });
    this[_trainManager].retrainModel({
      containerName: "pmtrainoutputs",
      inputs: inputs,
      outputs: outputs,
      callback: (err, jobId) => {
        //assert
        expect(err).to.not.be.ok; //no debe dar error
        this[_jobId] = jobId;
        done();
      }
    });
  }

  do({ done }) {
    const statusFinished = TrainManager.TrainingJobStatus.Finished;
    this[_trainManager].waitTrainingJobStatus({
      jobId: this[_jobId],
      status: statusFinished,
      callback: (err, results) => {
        //assert
        expect(err).to.not.be.ok; //no debe dar error
        done();
      }
    });
  }

}

module.exports = test;