const Unirest = require('unirest');
const RandomString = require('randomstring');
const ISDK = require('../../../libs/ml/sdk/ISDK');

const BatchScoreStatusCode = {
    NotStarted: "NotStarted",
    Running: "Running",
    Failed: "Failed",
    Cancelled: "Cancelled",
    Finished: "Finished"
};

//properties
const _jobsDict = Symbol("jobsDict");
const _prediction = Symbol("prediction");

//methods
const _startInterval = Symbol("startInterval");

const FakeSDK = class FakeSDK extends ISDK {

    /**
     * SDK de Azure ML
     * @param {Object} webService Objeto que define el servicio web del componente     
     * @param {string} webService.executeEndpoint Url de ejecución del servicio
     * @param {string} [webService.patchEndpoint] Url de ejecución del servicio
     * @param {string} webService.apiKey Clave de seguridad de la API
     * @param {string} webService.apiVersion Número de versión de la API     
     * @param {Object} [log] Objeto para el logeo de trazas
     * @param {Object} [prediction] Objeto para el logeo de trazas
     */
    constructor({ webService, log = null, prediction = null }) {
        super({ webService: webService, log: log });
        this[_jobsDict] = {};
        this[_prediction] = prediction;
    }

    [_startInterval]({ jobId, reqId = null }) {
        const interval = setInterval(() => { 
            const job = this[_jobsDict][jobId];
            let { counter, jobStatus } = job;
            const { limit } = job;
            const { StatusCode } = jobStatus;
            if(StatusCode === BatchScoreStatusCode.NotStarted || StatusCode === BatchScoreStatusCode.Running) {
                if(counter < limit) {
                    jobStatus = {
                        "StatusCode": "Running",
                        "Results":null,
                        "Details":null,
                        "CreatedAt": new Date().toISOString(),
                        "StartTime": new Date().toISOString(),
                        "EndTime": new Date(-8640000000000000).toISOString()
                    };
                } else {                    
                    const output = require('./output.json');
                    jobStatus = output;
                    clearInterval(interval);
                }  
            } else if(StatusCode === BatchScoreStatusCode.Cancelled || StatusCode === BatchScoreStatusCode.Failed) {
                clearInterval(interval);
            }
            counter += 1;
            this[_jobsDict][jobId].counter = counter;
            this[_jobsDict][jobId].jobStatus = jobStatus;
        }, 1000);
    }

    /**
     * Crea, pero no inicia, la tarea de reentramiento de un modelo en Azure ML
     * @param {Object[]} inputs Array de elementos de entrada para la tarea de reentrenamiento
     * @param {string} input.model Nombre del input dentro del experimento de Azure
     * @param {string} input.blobName Nombre del blob en Azure Storage
     * @param {Object[]} outputs Array de elementos de salida de la tarea de reentrenamiento
     * @param {string} output.model Nombre del output dentro del experimento de Azure
     * @param {string} output.blobName Nombre del blob en Azure Storage, donde se almacenará la salida
     * @param {Object} globalParameters Parámetros de la llamada REST
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {string} result Id de la tarea
     */
    createJob({ inputs, outputs, globalParameters = {}, callback, reqId = null }) {
        if(inputs == null) return callback("bad parameters [inputs] in createJob", null);
        if(outputs == null) return callback("bad parameters [outputs] in createJob", null);
        if(this._webService == null) return callback("webService missing", null);

        if(this._log != null) this._log.info({ message: "Submitting the job...", domain: "azure-ml-manager", reqId });
        let jobId = RandomString.generate(32).toLowerCase();
        this[_jobsDict][jobId] = {
            counter: 0,
            limit: Math.floor(Math.random() * 10) + 5
        }
        callback(null, jobId);
    }

    /**
     * Inicia la tarea de reentramiento de un modelo en Azure ML, a partir de su Id
     * @param {string} jobId Identificador de la tarea a iniciar
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result
     */
    startJob({ jobId, callback, reqId = null }) {
        if(jobId == null) return callback("bad parameters [jobId] in startJob", null);
        if(this._webService == null) return callback("webService missing", null);

        if(this._log != null) this._log.info({ message: `Starting the job [${jobId}]...`, domain: "azure-ml-manager", reqId });
        if(this[_jobsDict].hasOwnProperty(jobId)) {
            this[_jobsDict][jobId].jobStatus = {
                "StatusCode": "NotStarted",
                "Results":null,
                "Details":null,
                "CreatedAt": new Date().toISOString(),
                "StartTime": new Date().toISOString(),
                "EndTime": new Date(-8640000000000000).toISOString()
            };        
            this[_startInterval]({ jobId, reqId });
        }
        callback(null, undefined);
    }

    /**
     * Cancela una tarea de Azure ML, a partir de su Id
     * @param {string} jobId Identificador de la tarea a cancelar
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result
     */
    cancelJob({ jobId, callback, reqId = null }) {
        if(jobId == null) return callback("bad parameters [jobId] in cancelJob", null);
        if(this._webService == null) return callback("webService missing", null);

        if(this._log != null) this._log.info({ message: `Cancelling the job [${jobId}]...`, domain: "azure-ml-manager", reqId });
        this[_jobsDict][jobId].jobStatus = {
            "StatusCode": "Cancelled",
            "Results":null,
            "Details":null,
            "CreatedAt": new Date().toISOString(),
            "StartTime": new Date().toISOString(),
            "EndTime": new Date(-8640000000000000).toISOString()
        }; 
        callback(null, undefined);
    }

    /**
     * Consulta el estado o los resultados de una tarea
     * @param {string} jobId Identificador de la tarea a consultar
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Objeto que indica el estado de la tarea
     * @param {string} result.StatusCode Indicador del estado de la tarea [NotStarted, Running, Failed, Cancelled, Finished]
     * @param {Object} result.Results Objeto con los resultados, si los hay, de la tarea
     * @param {Object} result.Details Objeto de detalle
     */
    getJob({ jobId, callback, reqId = null }) {
        if(jobId == null) return callback("bad parameters [jobId] in getJob", null);
        if(this._webService == null) return callback("webService missing", null);

        let error = null;
        let result = null;
        if(this[_jobsDict].hasOwnProperty(jobId)) {
            const { jobStatus } = this[_jobsDict][jobId];                
            result = jobStatus;
        }
        callback(error, result);
    }

    /**
     * Parchea el modelo especificado del servicio web de Azure ML
     * @param {string} resourceName Nombre del recurso a parchear
     * @param {Object} blobFile Objeto con la información del blob en Azure Storage para parchear el servicio web
     * @param {string} blobFile.BaseLocation Url base del Azure Storage
     * @param {string} blobFile.RelativeLocation Ruta relativa del blob en Azure Storage
     * @param {string} blobFile.SasBlobToken Firma de acceso compartido (SAS) del blob en Azure Storage
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result
     */
    patchModel({ resourceName, blobFile, callback, reqId = null }) {
        if(resourceName == null) return callback("bad parameters [resourceName] in patchModel", null);
        if(blobFile == null) return callback("bad parameters [blobFile] in patchModel", null);
        if(this._webService == null) return callback("webService missing", null);

        const { BaseLocation, RelativeLocation, SasBlobToken } = blobFile;
        if(this._log != null) this._log.info({ message: `Patching trained model with blob [${RelativeLocation}]...`, domain: "azure-ml-manager", reqId });
        callback(null, undefined);       
    }

    /**
     * Realiza una predicción
     * @param {string} inputName Nombre del componente de entrada del servicio web
     * @param {Object} inputs Objeto con las entradas para realizar la predicción 
     * @param {Object} globalParameters Parámetros de la llamada REST
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Objeto que indica el resultado de la predicción
     */
    postPredictionRequest({ inputName, inputs, globalParameters, callback, reqId = null }) {
        if(inputName == null) return callback("bad parameters [inputName] in postPredictionRequest", null);
        if(inputs == null) return callback("bad parameters [inputs] in postPredictionRequest", null);
        if(this._webService == null) return callback("webService missing", null);
        
        callback(null, this[_prediction]);
    }

}

module.exports = FakeSDK;
module.exports.BatchScoreStatusCode = BatchScoreStatusCode;