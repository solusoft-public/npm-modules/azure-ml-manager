const IStorage = require('../../libs/storage/IStorage');
const Azure = require('azure-storage');

//properties
const _storage = Symbol("storage");

//methods
const _createEntity = Symbol("createEntity");

const FakeStorage = class FakeStorage extends IStorage {

    /**
     * Clase que realiza varias operaciones sobre Azure Storage
     * @param {string} accountName Nombre de la cuenta de Azure Storage
     * @param {string} key Clave de la cuenta de Azure Storage
     * @param {Object} [log] Objeto para el logeo de trazas
     */
    constructor({ accountName, key, log = null }) {
        super({ accountName: accountName, key: key, log: log });
        this[_storage] = {};
    }

    /**
     * Convierte un objeto en una entidad preparada para insertar en Azure Table Storage
     * @param {Object} entity Objeto a convertir en entidad
     * @param {string} [reqId] Id de correlación
     * @returns {Object} - Entidad construida
     */
    [_createEntity] ({ entity, reqId = null }) {
        if(entity == null) return callback("bad parameters [entity] in createEntity", null);

        let entGen = Azure.TableUtilities.entityGenerator;        
        for(let key of Object.keys(entity)) {
            let value = entity[key];
            if(value != null) {
                //Cuidado, un array o una fecha es un objeto también
                if(Array.isArray(value)) {
                    //Añadimos este else por si quisiéramos otro tratamiento para arrays en un futuro
                    entity[key] = entGen.String(value.toString());
                } else if(value instanceof Date) {
                    entity[key] = entGen.DateTime(value);
                } else if(typeof value === "object") {
                    entity[key] = entGen.String(JSON.stringify(value));
                } else if(typeof value === "boolean") {
                    entity[key] = entGen.Boolean(value);
                } else if(typeof value === "number") {
                    entity[key] = entGen.Double(value);
                } else {
                    entity[key] = entGen.String(value.toString());
                }  
            }            
        }
        return entity;
    }

    /**
     * Conjunto de entidades a subir, o reemplazar en caso de que la entidad exista, a una tabla de Azure Table Storage
     * @param {Object[]} entities Array de entidades a subir a la tabla
     * @param {string} tableName Nombre de la tabla
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Resultado de la operación
     */
    uploadEntitiesToTable({ entities, tableName, callback, reqId = null }) {
        if(entities == null) return callback("bad parameters [entities] in uploadEntitiesToTable", null);
        if(tableName == null) return callback("bad parameters [tableName] in uploadEntitiesToTable", null);
        if(this._accountName == null) return callback("accountName missing", null);
        if(this._key == null) return callback("key missing", null);

        if(this._log != null) this._log.info({ message: `Uploading the entities into table [${tableName}]...`, domain: "azure-storage-manager", reqId });        
        if(this[_storage][tableName] == null) this[_storage][tableName] = {};
        const _entities = entities.map(entity => this[_createEntity]({ entity, reqId }));
        for(let entity of _entities) {
            const { PartitionKey, RowKey } = entity;
            const key = PartitionKey._ + RowKey._;
            this[_storage][tableName][key] = entity;
        }
        callback(null, {});
    }

    /**
     * Realiza una consulta y devuelve sus resultados sobre una tabla de Azure Table Storage especificada por su nombre
     * @param {string} tableName Nombre de la tabla sobre la que realizar la consulta
     * @param {Object} query Consulta a realizar
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object[]} entities Array total de entidades recuperadas
     */
    queryTableEntities({ tableName, query, callback, reqId = null }) {
        if(tableName == null) return callback("bad parameters [tableName] in createJob", null);
        if(query == null) return callback("bad parameters [query] in createJob", null);
        if(this._accountName == null) return callback("accountName missing", null);
        if(this._key == null) return callback("key missing", null);

        const filter = query.toQueryObject().$filter;
        const regExp = new RegExp("PartitionKey eq '(.*)' and RowKey eq '(.*)'", "g");
        const match = regExp.exec(filter);
        const PartitionKey = match[1];
        const RowKey = match[2];
        const key = PartitionKey + RowKey;
        const entity = this[_storage].hasOwnProperty(tableName) ? this[_storage][tableName][key] : null;
        callback(null, [entity]);
    }

}

module.exports = FakeStorage;