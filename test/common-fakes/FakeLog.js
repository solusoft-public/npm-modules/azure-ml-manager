
const FakeLog = {
  trace({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { console.trace(message, payload != null ? payload : ""); },
  debug({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { console.log(message, payload != null ? payload : ""); },
  info({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { console.info(message, payload != null ? payload : ""); },
  warn({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { console.warn(message, payload != null ? payload : ""); },
  error({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { console.error(message, payload != null ? payload : ""); },
  fatal({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { console.error(message, payload != null ? payload : ""); },
  addResponseError({ level, message, code, res, domain = null, reason = null }) { },
};

module.exports = FakeLog;