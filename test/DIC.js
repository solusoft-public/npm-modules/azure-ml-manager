//Dependency Injection Container
//Clase que permite la inversión de control implementando un contenedor de dependencias

const FakeLog = require("./common-fakes/FakeLog.js");
const FakeStorage = require("./common-fakes/FakeStorage.js");

//properties
const _log = Symbol("log");
const _storage = Symbol("storage");
const _webServices = Symbol("webServices");
const _storageManager = Symbol("storageManager");

const DIC = class DIC {

  constructor({ storage, webServices }) {
    this[_storage] = storage;
    this[_webServices] = webServices;
  }

  getLog() {
    if (this[_log] == null) this[_log] = FakeLog;
    return this[_log];
  }

  getStorage() {
    if (this[_storage] == null) this[_storage] = {};
    return this[_storage];
  }

  getWebServices() {
    if (this[_webServices] == null) this[_webServices] = {};
    return this[_webServices];
  }

  getStorageManager() {
    if (this[_storageManager] == null) {
      const { accountName, key } = this.getStorage();
      this[_storageManager] = new FakeStorage({ accountName: accountName, key: key, log: this.getLog() });
    }
    return this[_storageManager];
  }

}

module.exports = DIC;