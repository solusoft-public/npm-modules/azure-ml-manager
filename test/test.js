const expect = require("chai").expect;
const Async = require('async');

describe("Entrenamiento y parcheo", function() {
  this.timeout(60000);

  it("Train:001 Entrenamiento de un modelo", done => { 
    const Test = require('./Train/test001/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);     
  });
  it("Train:002 Entrenamiento y parcheo de un modelo", done => { 
    const Test = require('./Train/test002/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);     
  });
  it("Train:003 Entrenamiento de un modelo de forma síncrona", done => { 
    const Test = require('./Train/test003/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);     
  });
  it("Train:004 Entrenamiento y parcheo de un modelo de forma síncrona", done => { 
    const Test = require('./Train/test004/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);     
  });
  it("Train:005 Parcheo de un modelo con id de job", done => {
    const Test = require('./Train/test005/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);    
  });
  it("Train:006 Obtención del estado de una tarea de reentrenamiento y parcheo", done => {
    const Test = require('./Train/test006/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);    
  });
  it("Train:007 Espera por el estado de una tarea de reentrenamiento", done => {
    const Test = require('./Train/test007/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);    
  });
  it("Train:008 Espera por el estado de una tarea de reentrenamiento y termina la espera cuando esta tarea se cancela", done => {
    const Test = require('./Train/test008/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);    
  });
  it("Train:009 Espera por el estado de una tarea de reentrenamiento y termina la espera cuando se cumple un número de reintentos", done => {
    const Test = require('./Train/test009/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);    
  });
});

describe("Predicción", function() {

  it("Predict:001 Predicción de un modelo componentes NO válidos", done => {
    const Test = require('./Predict/test012/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);    
  });
  it("Predict:002 Predicción de un modelo con múltiples componentes", done => {
    const Test = require('./Predict/test001/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);    
  });
  it("Predict:003 Predicción de un modelo con múltiples componentes y desambiguación", done => {
    const Test = require('./Predict/test002/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ],
    done);    
  });
  it("Predict:004 Prueba de getComponentsOutputs", function (done) { 
    const Test = require('./Predict/test009/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ], done);
  });
  it("Predict:005 Prueba de disambiguateVectors", function (done) { 
    const Test = require('./Predict/test010/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ], done);
  });
  it("Predict:006 Prueba de reduceVectors", function (done) { 
    const Test = require('./Predict/test011/test');
    const test = new Test({});
    Async.series([
      callback => test.before({ done: callback }),
      callback => test.do({ done: callback }),
      callback => test.after({ done: callback }),
    ], done);
  });
  describe("Componentes", function() {
    it("Components:001 Consulta a Azure Tables con parámetros NO válidos", function (done) { 
      const Test = require('./Predict/test006/test');
      const test = new Test({});
      Async.series([
        callback => test.before({ done: callback }),
        callback => test.do({ done: callback }),
        callback => test.after({ done: callback }),
      ], done);
    });
    it("Components:002 Consulta a Azure Tables", function (done) { 
      const Test = require('./Predict/test003/test');
      const test = new Test({});
      Async.series([
        callback => test.before({ done: callback }),
        callback => test.do({ done: callback }),
        callback => test.after({ done: callback }),
      ], done);
    });
    it("Components:003 Servicio web de Azure ML con parámetros NO válidos", function (done) { 
      const Test = require('./Predict/test007/test');
      const test = new Test({});
      Async.series([
        callback => test.before({ done: callback }),
        callback => test.do({ done: callback }),
        callback => test.after({ done: callback }),
      ], done);
    }); 
    it("Components:004 Servicio web de Azure ML", function (done) { 
      const Test = require('./Predict/test004/test');
      const test = new Test({});
      Async.series([
        callback => test.before({ done: callback }),
        callback => test.do({ done: callback }),
        callback => test.after({ done: callback }),
      ], done);
    }); 
  });
  describe("Ensembles", function() {
    it("Ensembles:001 Servicio web de Azure ML con parámetros NO válidos", function (done) { 
      const Test = require('./Predict/test008/test');
      const test = new Test({});
      Async.series([
        callback => test.before({ done: callback }),
        callback => test.do({ done: callback }),
        callback => test.after({ done: callback }),
      ], done);
    });
    it("Ensembles:002 Servicio web de Azure ML", function (done) { 
      const Test = require('./Predict/test005/test');
      const test = new Test({});
      Async.series([
        callback => test.before({ done: callback }),
        callback => test.do({ done: callback }),
        callback => test.after({ done: callback }),
      ], done);
    });
  });
     
});