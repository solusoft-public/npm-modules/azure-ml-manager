const AzureStorage = require('azure-storage');
const TableQuery = AzureStorage.TableQuery;

module.exports = {  
    Managers: {
        TrainManager: require('./libs/ml/train/MLTrainBESManager'),        
        PredictManager: require('./libs/ml/prediction/MLPredictManager')
    },
    Components: {
        IComponent: require("./libs/ml/prediction/component/IComponent"),
        AzureDataTable: require("./libs/ml/prediction/component/implementations/AzureDataTable"),
        AzureMLWebService: require("./libs/ml/prediction/component/implementations/AzureMLWebService")
    },
    Ensembles: {
        IEnsemble: require("./libs/ml/prediction/ensemble/IEnsemble"),
        AzureMLWebService: require("./libs/ml/prediction/ensemble/implementations/AzureMLWebService")
    },
    Log: {
        ILog: require("./libs/log/ILog")
    },
    SDK: {
        ISDK: require('./libs/ml/sdk/ISDK'),
        UnirestSDK: require('./libs/ml/sdk/implementations/UnirestSDK')
    },
    Storage: {
        IStorage: require('./libs/storage/IStorage'),
        StorageManager: require('./libs/storage/implementations/StorageManager'),
        TableQuery: TableQuery
    }
};