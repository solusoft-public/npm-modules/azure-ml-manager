De:	Andre Ferreira <Ferreira.Andre@microsoft.com>
Enviado el:	jueves, 2 de agosto de 2018 18:07
Para:	Adri�n Alonso Gonz�lez
CC:	MSSolve Case Email; Manuel Joaqu�n Garc�a S�nchez
Asunto:	RE: [REG:118080118700009] AML - Technical Questions about Machine 
Learning Classic / ARM

Hello Adri�n,

Thank you very much for sharing your questions with us.  Please see my answers below in blue:

- Reading the links that you provided us we didn't see any functional differences between classic 
and new web services. Are we right?
1) One functional difference is that with the new web service we don�t have an end point to new 
services;
2) Another difference is that you can copy web services across regions;
3) The new web service are ARM based so you can control with ARM and PowerShell for example.

- Is there a plan for deprecating the classic web services?
No, for now there is no plan to deprecate the classic web service.

- We don't use experiments with predictive web services. We have one experiment/classic web 
service for retraining and many experiments/classic web services for prediction. Now we create a 
second endpoint in our prediction web services for patching the model with the results for the 
retraining process (we used this guide: https://docs.microsoft.com/en-us/azure/machine-
learning/studio/retrain-a-classic-web-service). We didn't see this posibility with the new web 
services. If we want to adapt our process to new web services...What is the best way to proceed? 
We don't want to use cmdlets because we are using an Unix environment. Can we adapt on a easy 
way the patching process, maybe using the ARM Rest APIs?
- Yes, it is possible to retrain with new web services. 
Please see the document below to know how Retrain an existing predictive web service: 
https://docs.microsoft.com/en-us/azure/machine-learning/studio/retrain-existing-resource-
manager-based-web-service;

- You can retrain using Rest APIs, however, we don�t have any document showing how to this using 
Rest API. 
For more details about Azure Machine Learning Studio Management REST APIs please check: 
https://docs.microsoft.com/en-us/rest/api/machinelearning/.

If any of our scenarios are not useful, you can try using Azure Data Factory. Please see the link 
below to know more about how to  retrain and update Azure Machine Learning models with Azure 
Data Factory:
https://azure.microsoft.com/en-us/blog/retraining-and-updating-azure-machine-learning-models-
with-azure-data-factory/.

- Is there an usage limit for the classic web services? What about their prices? We now have about 
ten classic web services deployed but we don't have information about their price our limit usage. 
Indeed, we didn't have to create any web service plan.
To consult the price details please go to the following site https://azure.microsoft.com/en-
us/pricing/details/machine-learning-studio/ and look for �Production Web API pricing�.

 

Thank you very much for your attention.

Best Regards,
--
Andr� Ferreira
Support Engineer
Azure Big Data
Customer Services and Support
Office: 
+3512
10602
325
Ferreir
a.Andr
e@mic
rosoft.
com
9 AM - 
6 PM 
(GMT)
 

If you have any feedback about my work, please let either me or my manager Claudia Carvalho know at claudia.carvalho@microsoft.com

From: Adri�n Alonso Gonz�lez <aalonso@solusoft.es>  
Sent: 2 de agosto de 2018 11:16 
To: Andre Ferreira <Ferreira.Andre@microsoft.com> 
Cc: MSSolve Case Email <casemail@microsoft.com>; Manuel Joaqu�n Garc�a S�nchez 
<mgarcia@solusoft.es> 
Subject: RE: [REG:118080118700009] AML - Technical Questions about Machine Learning Classic / 
ARM

Hello Andre,

I'm Adri�n, nice to meet you too.

First of all thank you for the information. I also agree the scope statement.

We tried the new web services last week for first time and we realized that their patching process 
is different. So, now, we have the following doubts:

- Reading the links that you provided us we didn't see any functional differences between classic 
and new web services. Are we right?

- Is there a plan for deprecating the classic web services?

- We don't use experiments with predictive web services. We have one experiment/classic web 
service for retraining and many experiments/classic web services for prediction. Now we create a 
second endpoint in our prediction web services for patching the model with the results for the 
retraining process (we used this guide: https://docs.microsoft.com/en-us/azure/machine-
learning/studio/retrain-a-classic-web-service). We didn't see this posibility with the new web 
services. If we want to adapt our process to new web services...What is the best way to proceed? 
We don't want to use cmdlets because we are using an Unix environment. Can we adapt on a easy 
way the patching process, maybe using the ARM Rest APIs?

- Is there an usage limit for the classic web services? What about their prices? We now have about 
ten classic web services deployed but we don't have information about their price our limit usage. 
Indeed, we didn't have to create any web service plan.

We didn't know about the Azure Machine Learning Workbench, we'll take a look, it looks nice.

Thank you very much.

Best regards.


 
A
D
RI
�
N 
AL
ON
SO 
GO
NZ
�LE
Z
T�C
NIC
O 
DE 
INN
OVA
CI�
N
   9
02.6
78.2
08  
   9
1.68
8.66.
44  -
  w
ww.
solu
soft
.es 







De: Andre Ferreira <Ferreira.Andre@microsoft.com>  
Enviado el: jueves, 2 de agosto de 2018 10:41 
Para: Adri�n Alonso Gonz�lez <aalonso@solusoft.es> 
CC: MSSolve Case Email <casemail@microsoft.com>; Manuel Joaqu�n Garc�a S�nchez 
<mgarcia@solusoft.es> 
Asunto: RE: [REG:118080118700009] AML - Technical Questions about Machine Learning Classic / 
ARM

Hello Adri�n,

Nice to meet you. 

To know more details about the difference between classic and new Webservice, I kindly ask to 
please read the links below:
*	https://social.msdn.microsoft.com/Forums/azure/en-US/635eff71-b5c1-4c85-92fc-
750614c82b61/difference-between-classic-and-new-webservice?forum=MachineLearning; 
*	https://docs.microsoft.com/en-us/azure/machine-learning/studio/whats-new;

In case you might be starting to use Machine Learning in, we would like to suggest you to try to 
use Azure Machine Learning Workbench: 
Azure Machine Learning Workbench (preview) is an integrated, end-to-end data science and 
advanced analytics solution. It helps professional data scientists prepare data, develop 
experiments, and deploy models at cloud scale.
This service enables building, deploying, and managing machine learning and AI models using any 
Python tools and libraries. You can use a wide variety of data and compute services in Azure to 
store and process your data.
For more details about Azure Machine Learning Workbench I recommend to see the links below:
*	https://docs.microsoft.com/en-us/azure/machine-learning/service/ 
*	https://azure.microsoft.com/pt-pt/blog/diving-deep-into-what-s-new-with-azure-
machine-learning/.

Please let us  know if you would like me to reach you, or if the links above clarify your questions.

Thank you very much for your attention.

Best Regards,
--
Andr� Ferreira
Support Engineer
Azure Big Data
Customer Services and Support
Office: 
+3512
10602
325
Ferreir
a.Andr
e@mic
rosoft.
com
9 AM - 
6 PM 
(GMT)
 

If you have any feedback about my work, please let either me or my manager Claudia Carvalho know at claudia.carvalho@microsoft.com

From: Manuel Joaqu�n Garc�a S�nchez <mgarcia@solusoft.es>  
Sent: 2 de agosto de 2018 09:28 
To: Andre Ferreira <Ferreira.Andre@microsoft.com> 
Cc: MSSolve Case Email <casemail@microsoft.com>; Adri�n Alonso Gonz�lez 
<aalonso@solusoft.es> 
Subject: RE: [REG:118080118700009] AML - Technical Questions about Machine Learning Classic / 
ARM

Hello Andre,

I will transfer this support request to my coworker Adri�n Alonso, who has a more technical profile 
and he is more appropriate to manage this request. I will cc him on this email so he will answer 
you from now.

Regards,
Manuel J.



De: Andre Ferreira [mailto:Ferreira.Andre@microsoft.com]  
Enviado el: jueves, 02 de agosto de 2018 10:12 
Para: Manuel Joaqu�n Garc�a S�nchez <mgarcia@solusoft.es> 
CC: MSSolve Case Email <casemail@microsoft.com> 
Asunto: [REG:118080118700009] AML - Technical Questions about Machine Learning Classic / 
ARM

Hello Manuel,
 
Thank you for contacting Microsoft Support. My name is Andre Ferreira  and I am the Support 
Professional who will be working with you on your support request. You can reach me using the 
contact information in my signature.
As you've the preferred contact method as E-mail I will work with you via email for this support 
case, please find below a summary of the problem scope for this support request. It details what I 
will focus on and under what specific conditions we will consider the request resolved. Please let 
me know if anything is inaccurate, missing or you disagree with the scope statement.
 
If the Microsoft products involved are found to be working as intended, or the cause of the issue is 
determined to be the result of a third-party application we will assist in finding a resolution. 
However, if no solution can be found this support request will be considered resolved.
 
ISSUE
Customer problem description:
"We need to know the different technical characteristics between Machine Learning's Classic 
services and Machine Learning's ARM services.
We also need you to help us migrate Classic Machine Learning services to ARM. " 

IMPACT
Severity C - Important.
 
SCOPE
The case can be considered closed once we accomplished one of the following conditions:
- We are able to provide a solution/workaround to the error message above on the current 
customer environment.
- We detect the issue is a bug on the product and we have an answer from escalation on if we can 
provide a fix or not.
- The issue is by design.
- The issue is caused by third party providers. In that case you would need to engage the third 
party support to get a solution on the issue above, or would like to change something, please let 
me know as soon as possible. If you have any questions, please let me know.
 
MORE INFORMATION:
I�ll contact you in the next 20 minutes.

Thank you very much for your comprehension and sorry for the inconvenience.
 
Best Regards,
--
Andr� Ferreira
Support Engineer
Azure Big Data
Customer Services and Support
Office: 
+3512
10602
325
Ferreir
a.Andr
e@mic
rosoft.
com
9 AM - 
6 PM 
(GMT)
 

If you have any feedback about my work, please let either me or my manager Claudia Carvalho know at claudia.carvalho@microsoft.com


 
Microsoft se compromete a proteger su privacidad. Por favor lea el Microsoft Privacy Statement 
para m�s informaci�n. 
El presente es un correo para un caso de soporte de Microsoft Corp. RESPONDA A TODOS o 
INCLUYA la direcci�n casemail@microsoft.com EN SU RESPUESTA si quiere que su respuesta se 
agregue al caso autom�ticamente.   
Gracias.
