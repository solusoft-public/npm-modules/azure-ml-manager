const Azure = require('azure-storage');
const Async = require('async');
const IStorage = require('../../IStorage');

//methods
const _listBlobsPagedInContainer = Symbol("listBlobsPagedInContainer");
const _createEntity = Symbol("createEntity");
const _queryTableEntities = Symbol("queryTableEntities");
const _createBatch = Symbol("createBatch");


const StorageManager = class StorageManager extends IStorage {

    /**
     * Clase que realiza varias operaciones sobre Azure Storage
     * @param {string} accountName Nombre de la cuenta de Azure Storage
     * @param {string} key Clave de la cuenta de Azure Storage
     * @param {Object} [log] Objeto para el logeo de trazas
     */
    constructor({ accountName, key, log = null }) {
        super({ accountName: accountName, key: key, log: log })
    }
    
    /**
     * Lista, de forma recursiva, todos los blobs contenidos en un contenedor de Azure
     * @param {string} storageContainerName Nombre del contenedor     
     * @param {Object[]} blobs Array de blobs listados hasta ahora
     * @param {string} [continuationToken] Token de continuación, para listar los blobs de forma paginada
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object[]} blobs Array total de blobs listados
     */
    [_listBlobsPagedInContainer] ({ storageContainerName, blobs, continuationToken = null, callback, reqId = null }) {
        if(storageContainerName == null) return callback("bad parameters [storageContainerName] in listBlobsPagedInContainer", null);        
        if(blobs == null) return callback("bad parameters [outputs] in listBlobsPagedInContainer", null);
        if(this._accountName == null) return callback("accountName missing", null);
        if(this._key == null) return callback("key missing", null);

        let blobService = Azure.createBlobService(this._accountName, this._key);
        blobService.listBlobsSegmented(storageContainerName, continuationToken, (err, data) => {
            if(err) {
                callback(err);
            } else {
                blobs = blobs.concat(data.entries);
                if (data.continuationToken !== null) {
                    this[_listBlobsPagedInContainer]({ storageContainerName, continuationToken: data.continuationToken, blobs, reqId, callback });
                } else {
                    callback(null, blobs);
                }
            }
        });
    }

    /**
     * Convierte un objeto en una entidad preparada para insertar en Azure Table Storage
     * @param {Object} entity Objeto a convertir en entidad
     * @param {string} [reqId] Id de correlación
     * @returns {Object} - Entidad construida
     */
    [_createEntity] ({ entity, reqId = null }) {
        if(entity == null) return callback("bad parameters [entity] in createEntity", null);

        let entGen = Azure.TableUtilities.entityGenerator;        
        for(let key of Object.keys(entity)) {
            let value = entity[key];
            if(value != null) {
                //Cuidado, un array o una fecha es un objeto también
                if(Array.isArray(value)) {
                    //Añadimos este else por si quisiéramos otro tratamiento para arrays en un futuro
                    entity[key] = entGen.String(value.toString());
                } else if(value instanceof Date) {
                    entity[key] = entGen.DateTime(value);
                } else if(typeof value === "object") {
                    entity[key] = entGen.String(JSON.stringify(value));
                } else if(typeof value === "boolean") {
                    entity[key] = entGen.Boolean(value);
                } else if(typeof value === "number") {
                    entity[key] = entGen.Double(value);
                } else {
                    entity[key] = entGen.String(value.toString());
                }  
            }            
        }
        return entity;
    }

    /**
     * Crea un batch de objetos para insertar en Azure Table Storage
     * @param {Object[]} entities Array de objetos a insertar en Azure Table Storage. El tamaño máximo de un batch en Azure es de 100
     * @param {string} [reqId] Id de correlación
     * @returns {Object} - Objeto Azure.TableBatch
     */
    [_createBatch] ({ entities, reqId = null }) {
        if(entities == null) return callback("bad parameters [entities] in createBatch", null);

        let batch = new Azure.TableBatch();
        for(let i = 0; i < entities.length; i++) {
            let entity = this[_createEntity]({ entity: entities[i], reqId });
            batch.insertOrReplaceEntity(entity);
        }
        return batch;
    }
    
    /**
     * Realiza una consulta y devuelve sus resultados, mediante llamadas recursivas, sobre una tabla de Azure Table Storage, especificada por su nombre
     * @param {string} tableName Nombre de la tabla sobre la que realizar la consulta
     * @param {Object} query Consulta a realizar
     * @param {Object[]} entities Array de entidades recuperadas hasta ahora
     * @param {string} [continuationToken] Token utilizado para consultar los resultados de la query de forma paginada
     * @param {string} [reqId] Id de correlación     
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object[]} entities Array total de entidades recuperadas
     */
    [_queryTableEntities] ({ tableName, query, entities, continuationToken = null, callback, reqId = null }) {
        if(tableName == null) return callback("bad parameters [tableName] in queryTableEntities", null);
        if(query == null) return callback("bad parameters [query] in queryTableEntities", null);
        if(entities == null) return callback("bad parameters [entities] in queryTableEntities", null);
        if(this._accountName == null) return callback("accountName missing", null);
        if(this._key == null) return callback("key missing", null);

        let tableService = Azure.createTableService(this._accountName, this._key);
        tableService.queryEntities(tableName, query, continuationToken, (err, data) => {
            if(err) {
                callback(err);
            } else {
                entities = entities.concat(data.entries);
                if (data.continuationToken !== null) {
                    this[_queryTableEntities]({ tableName, query, continuationToken: data.continuationToken, entities, reqId, callback });
                } else {
                    callback(null, entities);
                }
            }
        });
    }    
        
    /**
     * Crea un contenedor en Azure Storage, en caso de que no exista previamente
     * @param {string} containerName Nombre del contenedor
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Resultado de la operación
     * @param {Object} response Respuesta de la llamada a la API REST
     */
    createContainerIfNotExists({ containerName, callback, reqId = null }) {
        if(containerName == null) return callback("bad parameters [containerName] in createContainerIfNotExists", null, null);
        if(this._accountName == null) return callback("accountName missing", null, null);
        if(this._key == null) return callback("key missing", null, null);

        let blobService = Azure.createBlobService(this._accountName, this._key);
        blobService.createContainerIfNotExists(containerName, callback);
    }

    /**
     * Lista todos los blobs contenidos en un contenedor de Azure Storage
     * @param {string} storageContainerName Nombre del contenedor
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object[]} blobs Array total de blobs listados
     */
    listBlobsInContainer({ storageContainerName, callback, reqId = null }) {
        if(storageContainerName == null) return callback("bad parameters [storageContainerName] in listBlobsInContainer", null);
        if(this._accountName == null) return callback("accountName missing", null);
        if(this._key == null) return callback("key missing", null);

        let blobs = [];
        this[_listBlobsPagedInContainer]({ storageContainerName, blobs, reqId, callback });
    }
        
    /**
     * Elimina todos los blobs de un contenedor excepto los que tiene una extensión concreta
     * @param {string} storageContainerName Nombre del contenedor
     * @param {string[]} extensions Array de extensiones que no serán borradas
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Resultado de la operación
     * @param {Object} response Respuesta de la llamada a la API REST
     */
    deleteAllBlobsExcept({ storageContainerName, extensions, callback, reqId = null }) {
        if(storageContainerName == null) return callback("bad parameters [storageContainerName] in deleteAllBlobsExcept", null, null);
        if(extensions == null) return callback("bad parameters [extensions] in deleteAllBlobsExcept", null, null);
        if(this._accountName == null) return callback("accountName missing", null, null);
        if(this._key == null) return callback("key missing", null, null);

        if(this._log != null) this._log.info({ message: "Cleaning the storage...", domain: "azure-storage-manager", reqId });
        Async.waterfall(
            [
                (callback) => { this.listBlobsInContainer({ storageContainerName, reqId, callback }); },
                (blobs, callback) => {
                    Async.each(
                        blobs,
                        (blob, callback) => {
                            let blobContentType = blob.contentSettings.contentType;
                            let blobName = blob.name;
                            //Este método podría provocar resultados inesperados con archivos ocultos o sin nombre (Ej. ".gitignore"), o sin extensión. No obstante para el caso que nos ocupa este método es válido
                            let blobExtension = blobName.split('.').pop();
                            let matched = extensions.find(extension => extension === blobExtension);
                            if(matched == null) {
                                let blobService = Azure.createBlobService(this._accountName, this._key);
                                if(this._log != null) this._log.info({ message: `Deleting the blob [${blobName}]...`, domain: "azure-storage-manager", reqId });
                                blobService.deleteBlobIfExists(storageContainerName, blobName, callback);
                            }
                            else {
                                callback(null, null);
                            }
                        },
                        callback
                    );
                }
            ],
            callback
        );
    }
    
    /**
     * Crea una tabla de Azure Table Storage, en caso de que no exista previamente
     * @param {string} tableName Nombre de la tabla
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Resultado de la operación
     * @param {Object} response Respuesta de la llamada a la API REST
     */
    createTableIfNotExists({ tableName, callback, reqId = null }) {
        if(tableName == null) return callback("bad parameters [tableName] in createTableIfNotExists", null, null);
        if(this._accountName == null) return callback("accountName missing", null, null);
        if(this._key == null) return callback("key missing", null, null);

        let tableService = Azure.createTableService(this._accountName, this._key);
        tableService.createTableIfNotExists(tableName, callback);
    }

    /**
     * Conjunto de entidades a subir, o reemplazar en caso de que la entidad exista, a una tabla de Azure Table Storage
     * @param {Object[]} entities Array de entidades a subir a la tabla
     * @param {string} tableName Nombre de la tabla
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Resultado de la operación
     */
    uploadEntitiesToTable({ entities, tableName, callback, reqId = null }) {
        if(entities == null) return callback("bad parameters [entities] in uploadEntitiesToTable", null);
        if(tableName == null) return callback("bad parameters [tableName] in uploadEntitiesToTable", null);
        if(this._accountName == null) return callback("accountName missing", null);
        if(this._key == null) return callback("key missing", null);

        if(this._log != null) this._log.info({ message: `Uploading the entities into table [${tableName}]...`, domain: "azure-storage-manager", reqId });        
        let tableService = Azure.createTableService(this._accountName, this._key);
        Async.series(
            [
                callback =>  this.createTableIfNotExists({ tableName, reqId, callback }),
                callback => {
                    let page = 0;
                    let pageSize = 100;
                    Async.doUntil(
                        done => {
                            let start = page * pageSize;
                            let end = start + pageSize;
                            let entitiesPage = entities.slice(start, end);                
                            let batch = this[_createBatch]({ entities: entitiesPage, reqId });
                            tableService.executeBatch(tableName, batch, 
                                (err, result, response) => {
                                    done(err, entitiesPage);
                                }
                            );
                        },
                        newData => {
                            //La última página es aquella cuyo tamaño es menor que el tamaño de página
                            let isLast = Boolean(!newData || newData.length < pageSize);
                            if(!isLast) page++;
                            return isLast;
                        },
                        callback            
                    );                    
                },
            ],
            callback
        );
    }

    /**
     * Realiza una consulta y devuelve sus resultados sobre una tabla de Azure Table Storage especificada por su nombre
     * @param {string} tableName Nombre de la tabla sobre la que realizar la consulta
     * @param {Object} query Consulta a realizar
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object[]} entities Array total de entidades recuperadas
     */
    queryTableEntities({ tableName, query, callback, reqId = null }) {
        if(tableName == null) return callback("bad parameters [tableName] in queryTableEntities", null);
        if(query == null) return callback("bad parameters [query] in queryTableEntities", null);
        if(this._accountName == null) return callback("accountName missing", null);
        if(this._key == null) return callback("key missing", null);

        let entities = [];
        this[_queryTableEntities]({ tableName, query, entities, reqId, callback });
    }

}

module.exports = StorageManager;