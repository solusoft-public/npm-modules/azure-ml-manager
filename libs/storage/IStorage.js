const IStorage = class IStorage {

    /**
     * Clase que realiza varias operaciones sobre Azure Storage
     * @param {string} accountName Nombre de la cuenta de Azure Storage
     * @param {string} key Clave de la cuenta de Azure Storage
     * @param {Object} [log] Objeto para el logeo de trazas
     */
    constructor({ accountName, key, log = null }) {
        this._accountName = accountName;
        this._key = key;
        this._log = log || global.log;
    }

    getAccountName() {
        return this._accountName;
    }

    getKey() {
        return this._key;
    }

    /** Conjunto de entidades a subir, o reemplazar en caso de que la entidad exista, a una tabla de Azure Table Storage
     * @param {Object[]} entities Array de entidades a subir a la tabla
     * @param {string} tableName Nombre de la tabla
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Resultado de la operación
     */
    uploadEntitiesToTable({ entities, tableName, callback, reqId = null }) {
        throw new Error('Not yet implemented');
    }

    /**
     * Realiza una consulta y devuelve sus resultados sobre una tabla de Azure Table Storage especificada por su nombre
     * @param {string} tableName Nombre de la tabla sobre la que realizar la consulta
     * @param {Object} query Objeto Azure.TableQuery a utilizar para consultar la tabla de Azure
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object[]} entities Array total de entidades recuperadas
     */
    queryTableEntities({ tableName, query, callback, reqId = null }) {
        throw new Error('Not yet implemented');
    }

}

module.exports = IStorage;