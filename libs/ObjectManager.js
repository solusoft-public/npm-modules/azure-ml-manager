//properties
const _log = Symbol("log");


const ObjectManager = class ObjectManager {

    /**
     * Clase para el manejo de objetos javascript
     * @param {Object} [log] Objeto para el logeo de trazas
     */
    constructor({ log = null }) {
        this[_log] = log || global.log;
    }

    /**
     * Construye un objeto a partir de un array de claves y un array de valores
     * @param {string[]} keys Array de claves
     * @param {string[]} values Array de valores
     * @returns {Object} - Objeto construido
     */
    buildObject({ keys, values }) {
        let object = null;
        try {
            object = keys.reduce((obj, key, index) => {
                if(key != null) obj[key] = values[index];
                return obj;
            }, {});
        } catch(err) {}
        return object;
    }
     
    /**
     * Obtiene un subconjunto de un objeto, únicamente con las claves especificadas
     * @param  {Object} object Objeto sobre el que realizar el subconjunto
     * @param  {string[]} keys Array de claves para obtener el subconjunto del objeto
     * @returns {Object} - Objeto con el subconjunto de claves
     */
    subsetObject({ object, keys }) {
        let _object = null;
        try {
            _object = Object.keys(object).reduce((obj, key, index) => {
                if(key != null && keys.includes(key)) {
                    obj[key] = object[key];
                }
                return obj;
            }, {});
        } catch(err) {}
        return _object;
    }

}

module.exports = ObjectManager;