const ILog = class ILog {

    /**
     * Sirve para generar un log que tendrá nivel trace. Registro de bibliotecas externas usadas por su aplicación o registro de aplicaciones
     * muy detallado
     * @param  {string} message Es el mensaje que tendrá su traza
     * @param  {string} [reqId] Es el identificador de su traza. Si no lo especifica se autogenera
     * @param  {number} [code] Es el código de la respuesta a una petición
     * @param  {string} [domain] Es el dominio que hizo una determinada petición. Podría ser el identificador de un proveedor, el módulo que 
     * hizo la petición...
     * @param  {string} [reason] Es la razón por la que se produjo la traza
     * @param  {Object} [payload] Un objeto arbitrario, en JSON, que incluye informacion adicional sobre la traza
     */
    trace({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { 
        throw new Error('Not yet implemented');
    }

    /**
     * Sirve para generar un log que tendrá nivel debug. Es cualquier otra cosa, es decir, es incluso demasiado detallado para ser info
     * @param  {string} message Es el mensaje que tendrá su traza
     * @param  {string} [reqId] Es el identificador de su traza. Si no lo especifica se autogenera
     * @param  {number} [code] Es el código de la respuesta a una petición
     * @param  {string} [domain] Es el dominio que hizo una determinada petición. Podría ser el identificador de un proveedor, el módulo que 
     * hizo la petición...
     * @param  {string} [reason] Es la razón por la que se produjo la traza
     * @param  {Object} [payload] Un objeto arbitrario, en JSON, que incluye informacion adicional sobre la traza
     */
    debug({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { 
        throw new Error('Not yet implemented'); 
    }

    /**
     * Sirve para generar un log que tendrá nivel info. Es para expresar detalles sobre una operación determinada
     * @param  {string} message Es el mensaje que tendrá su traza
     * @param  {string} [reqId] Es el identificador de su traza. Si no lo especifica se autogenera
     * @param  {number} [code] Es el código de la respuesta a una petición
     * @param  {string} [domain] Es el dominio que hizo una determinada petición. Podría ser el identificador de un proveedor, el módulo que 
     * hizo la petición...
     * @param  {string} [reason] Es la razón por la que se produjo la traza
     * @param  {Object} [payload] Un objeto arbitrario, en JSON, que incluye informacion adicional sobre la traza
     */
    info({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { 
        throw new Error('Not yet implemented'); 
    }

    /**
     * Sirve para generar un log que tendrá nivel warn. Es un aviso de que es probable que la parte señalada deba ser revisada
     * @param  {string} message Es el mensaje que tendrá su traza
     * @param  {string} [reqId] Es el identificador de su traza. Si no lo especifica se autogenera
     * @param  {number} [code] Es el código de la respuesta a una petición
     * @param  {string} [domain] Es el dominio que hizo una determinada petición. Podría ser el identificador de un proveedor, el módulo que 
     * hizo la petición...
     * @param  {string} [reason] Es la razón por la que se produjo la traza
     * @param  {Object} [payload] Un objeto arbitrario, en JSON, que incluye informacion adicional sobre la traza
     */
    warn({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { 
        throw new Error('Not yet implemented');
    }

    /**
     * Sirve para generar un log que tendrá nivel error. Indica que hubo un error en un/a método/petición de la aplicación pero el servicio/app 
     * puede continuar con su funcionamiento recibiendo otras peticiones
     * @param  {string} message Es el mensaje que tendrá su traza
     * @param  {string} [reqId] Es el identificador de su traza. Si no lo especifica se autogenera
     * @param  {number} [code] Es el código de la respuesta a una petición
     * @param  {string} [domain] Es el dominio que hizo una determinada petición. Podría ser el identificador de un proveedor, el módulo que 
     * hizo la petición...
     * @param  {string} [reason] Es la razón por la que se produjo la traza
     * @param  {Object} [payload] Un objeto arbitrario, en JSON, que incluye informacion adicional sobre la traza
     */
    error({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { 
        throw new Error('Not yet implemented');
    }

    /**
     * Sirve para generar un log que tendrá nivel fatal. Este error se usa cuando el servicio o aplicación se ha detenido o ha llegado a ser 
     * inutilizable y se ha roto
     * @param  {string} message Es el mensaje que tendrá su traza
     * @param  {string} [reqId] Es el identificador de su traza. Si no lo especifica se autogenera
     * @param  {number} [code] Es el código de la respuesta a una petición
     * @param  {string} [domain] Es el dominio que hizo una determinada petición. Podría ser el identificador de un proveedor, el módulo que 
     * hizo la petición...
     * @param  {string} [reason] Es la razón por la que se produjo la traza
     * @param  {Object} [payload] Un objeto arbitrario, en JSON, que incluye informacion adicional sobre la traza
     */
    fatal({ message, reqId = null, code = null, domain = null, reason = null, payload = null }) { 
        throw new Error('Not yet implemented');
    }

    /**
     * Añade un error que llegará al usuario 
     * @param {String} level - nivel del error "trace", "info", "debug", "error", "fatal", "warn"
     * @param {String} message mensaje de traza que se registrara
     * @param {Integer} code, codigo HTTP o Custom que identifica el estado de la operación
     * @param {String} domain Opcional, identificador del dominio del error, donde se origino
     * @param {String} reason Opcional, explicación de la razón por la que se originó el error
     * @param {Object} res - referencia a la response donde se utilizara locals.error y locals.errors para almacenar los errores
     */
    addResponseError({ level, message, code, res, domain = null, reason = null }) {
        throw new Error('Not yet implemented');
    };

}

module.exports = ILog;