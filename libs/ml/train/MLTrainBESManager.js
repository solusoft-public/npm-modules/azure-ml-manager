const Unirest = require('unirest');
const Async = require('async');
const AzureStorage = require('azure-storage');

const TableQuery = AzureStorage.TableQuery;


//properties
const _storageManager = Symbol("storageManager");
const _sdk = Symbol("sdk");
const _log = Symbol("log");

//methods
const _updatePatchJobStatus = Symbol("updatePatchJobStatus");
const _doTrain = Symbol("doTrain");
const _doWaitTask = Symbol("doWaitTask");
const _patchModelWithResults = Symbol("patchModelWithResults");

const TrainingJobStatus = {
    NotStarted: "NotStarted",
    Running: "Running",
    Failed: "Failed",
    Cancelled: "Cancelled",
    Finished: "Finished"
};

const PatchJobStatus = {
    NotStarted: "NotStarted",
    Running: "Running",
    Failed: "Failed",
    Finished: "Finished"
};

const MLTrainBESManager = class MLTrainBESManager {

    /**
     * Clase encargada de gestionar el reentrenamiento de los modelos de Azure ML
     * @param {Object} storageManager Instancia del módulo encargado de gestionar las operaciones sobre Azure Storage
     * @param {Object} azureMLSDK Instancia de AzureMLSdk
     * @param {Object} [log] Objeto para el logeo de trazas
     */
    constructor({ storageManager, azureMLSDK, log = null }) {
        this[_storageManager] = storageManager;
        this[_sdk] = azureMLSDK;
        this[_log] = log || global.log;
    }

    /**
     * Actualiza el estado de una tarea
     * @param {string} jobId Identificador de la tarea
     * @param {string} status Estado de la tarea
     * @param {Date} [EndTime] Fecha de fin de la tarea
     * @param {Object} [detail] Objeto que especifica el detalle del estado de una tarea
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Resultado de la operación
     */
    [_updatePatchJobStatus]({ jobId, patchStatus, endTime = null, detail = null, callback, reqId = null }) {
        const entity = { 
            PartitionKey: "azure-ml-manager",
            RowKey: jobId,
            patchStatus,
            detail
        };
        if(endTime != null) entity.endTime = endTime;

        this[_storageManager].uploadEntitiesToTable({ entities: [entity], tableName: "patchJobsStatus", reqId, callback });
    }

    /**
     * Realiza todo el flujo necesario para reentrenar un modelo de Azure ML: crea la tarea, la inicia y espera a que esté en estado Finished (finalizado)
     * @param {string} containerName Nombre del contenedor donde se almacenarán los resultados del reentrenamiento
     * @param {Object[]} inputs Array de elementos de entrada para la tarea de reentrenamiento
     * @param {string} input.model Nombre del input dentro del experimento de Azure
     * @param {string} input.blobName Nombre del blob en Azure Storage
     * @param {Object[]} outputs Array de elementos de salida de la tarea de reentrenamiento
     * @param {string} output.model Nombre del output dentro del experimento de Azure
     * @param {string} output.blobName Nombre del blob en Azure Storage, donde se almacenará la salida
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {string} jobId Identificador de la tarea de entrenamiento creada
     */
    [_doTrain]({ containerName, inputs, outputs, callback, reqId = null }) {
        const trFunctionInputs = section => {
            //Transformamos el array en objeto mediante programación funcional                               
            let inputs = section
                .map(input => {
                    return {
                        [input.model]: {
                            ConnectionString: connectionString,
                            RelativeLocation: `${savePath}${input.blobName}`,
                        }
                    }
                })
                .reduce((prev, curr) => {
                    let key = Object.keys(curr)[0];
                    prev[key] = curr[key];            
                    return prev;
                }, {});                                    
            return inputs;
        };
    
        const accountName = this[_storageManager].getAccountName();
        const key = this[_storageManager].getKey();
        const connectionString = `DefaultEndpointsProtocol=https;AccountName=${accountName};AccountKey=${key}`;
        let basePath = "/";                
        const savePath = `${containerName}${basePath}`;
    
        let jobId;
        Async.waterfall(
            [
                callback => {                                 
                    const _inputs = trFunctionInputs(inputs);
                    const _outputs = trFunctionInputs(outputs);
                    //Creamos el job que realizará el entrenamiento
                    this[_sdk].createJob({ globalParameters: {}, inputs: _inputs, outputs: _outputs, reqId, callback });
                },
                (result, callback) => { 
                    jobId = result;
                    //Iniciamos el job que realizará el entrenamiento
                    this[_sdk].startJob({ jobId, reqId, callback: (err, result) => {
                        callback(err, jobId);
                    }});
                },
                (jobId, callback) => {                    
                    this[_updatePatchJobStatus]({ jobId, patchStatus: PatchJobStatus.NotStarted, reqId, callback: (err, result) => {
                        callback(err, jobId);
                    }});
                }
            ],
            (err, result) => {
                callback(err, jobId);
            }
        );
    }

    /**
     * Método auxiliar que consulta cada cierto tiempo si una tarea ha alcanzado el estado deseado
     * @param {string} jobId Identificador de la tarea por la que esperar
     * @param {TrainingJobStatus} status Estado de la tarea por el que esperar
     * @param {number} timeStep Incremento de tiempo entre cada consulta por el estado de la tarea
     * @param {number} attemps Número de consultas restantes por hacer
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Objeto que indica el estado de la tarea
     * @param {string} result.StatusCode Indicador del estado de la tarea [NotStarted, Running, Failed, Cancelled, Finished]
     * @param {Object} result.Results Objeto con los resultados, si los hay, de la tarea
     * @param {Object} result.Details Objeto de detalle
     */
    [_doWaitTask]({ jobId, status, timeStep, attemps, callback, reqId = null }) {
        let _attemps = attemps;
        setTimeout(() => {
            this.getJobStatus({ jobId, reqId, callback: (err, response) => {
                _attemps -= 1;
                let resStatus;
                //No miramos valores negativos. De esta forma attemps = 0 equivale a intentos infinitos
                let isLastAttemp = _attemps == 0;
                if(err == null) {
                    const { training } = response;
                    resStatus = training.StatusCode;  
                    if(this[_log] != null) this[_log].info({ message: `Job [${jobId}] status [${resStatus}]`, domain: "azure-ml-manager", reqId });
                }
                const failedOrCancelled = (resStatus === TrainingJobStatus.Cancelled || resStatus === TrainingJobStatus.Finished);
                if(err != null || isLastAttemp || resStatus == status || failedOrCancelled) {
                    callback(err, response);
                } else {
                    this[_doWaitTask]({ jobId, status, timeStep, attemps: _attemps, reqId, callback });
                }
            }}); 
        }, timeStep);
    }

    /**
     * Consulta el estado de una tarea de reentrenamiento finalizada y parchea (actualiza) todos los servicios webs especificados
     * @param {Object[]} patchList Objetos con la información de los modelos a parchear
     * @param {Object} patchList.sdk Instancia del SDK de Azure ML
     * @param {string} patchList.outputName Nombre del componente de salida del servicio web del modelo reentrenado
     * @param {string} patchList.resourceName Nombre del modelo reentrenado durante su exportación
     * @param {Object} trainResults Objeto con los resultados del proceso de entrenamiento
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object[]} results Resultados de la tarea de reentrenamiento y parcheo
     */
    [_patchModelWithResults]({ patchList, trainResults, callback, reqId = null }) {                
        if(patchList == null) return callback("bad parameters [patchList] in patchModelWithResults", null);
        if(trainResults == null) return callback("bad parameters [trainResults] in patchModelWithResults", null);        
        
        Async.each(
            patchList,
            (patchObject, callback) => {
                const { resourceName, outputName, sdk } = patchObject;
                if(!trainResults.hasOwnProperty(outputName)) return callback(`wrong model name: ${outputName}`, null);
                         
                sdk.patchModel({ resourceName, blobFile: trainResults[outputName], reqId, callback });
            },
            callback
        );         
    }

    /**
     * Consulta el estado o los resultados de una tarea
     * @param {string} jobId Identificador de la tarea a consultar
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Objeto que indica el estado de la tarea
     * @param {Object} result.training Objeto que indica el estado de la tarea de entrenamiento
     * @param {string} result.training.StatusCode Indicador del estado de la tarea [NotStarted, Running, Failed, Cancelled, Finished]
     * @param {Object} result.training.Results Objeto con los resultados, si los hay, de la tarea
     * @param {Object} result.training.Details Objeto de detalle
     * @param {Object} result.patch Objeto que indica el estado de la tarea de parcheo
     * @param {string} result.patch.StatusCode Indicador del estado de la tarea [NotStarted, Running, Failed, Finished]     
     * @param {Object} result.patch.Details Objeto de detalle
     */
    getJobStatus({ jobId, callback, reqId = null }) {        
        Async.parallel(
            [
                callback => this[_sdk].getJob({ jobId, reqId, callback }), //Obtenemos el estado del entrenamiento
                callback => {
                    //Obtenemos el estado del parcheo
                    Async.waterfall(
                        [
                            callback => {
                                const query = new TableQuery()
                                    .where('PartitionKey eq ? and RowKey eq ?', "azure-ml-manager", jobId);
                                this[_storageManager].queryTableEntities({ tableName: "patchJobsStatus", query, reqId, callback });
                            },
                            (patchJobStatus, callback) => {
                                let result = null;
                                if(patchJobStatus == null || !(Array.isArray(patchJobStatus)) || patchJobStatus.length == 0) return callback("bad patchJobStatus", result);
                                
                                const { patchStatus, detail, endTime } = patchJobStatus[0];
                                result = {
                                    StatusCode: patchStatus._,
                                    Details: detail != null ? detail._ : null,
                                    EndTime: endTime != null ? endTime._.toISOString() : null                                    
                                };
                                callback(null, result);
                            }
                        ],
                        callback
                    );
                }
            ],
            (err, results) => {
                let result = null;
                if(err == null && Array.isArray(results) && results.length >= 2) {
                    result = {
                        training: results[0],
                        patch: results[1]
                    };
                }
                callback(err, result);
            }
        );                
    }

    /**
     * Consulta cada cierto tiempo el estado de la tarea hasta que, esta alcanza el estado esperado, o se alcanza el número máximo de intentos
     * @param {string} jobId Identificador de la tarea por la que esperar
     * @param {TrainingJobStatus} status Estado de la tarea por el que esperar
     * @param {number} [timeStep=5000] Incremento de tiempo entre cada consulta por el estado de la tarea
     * @param {number} [attemps=0] Número máximo de consultas
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Objeto que indica el estado de la tarea
     * @param {string} result.StatusCode Indicador del estado de la tarea [NotStarted, Running, Failed, Cancelled, Finished]
     * @param {Object} result.Results Objeto con los resultados, si los hay, de la tarea
     * @param {Object} result.Details Objeto de detalle
     */
    waitTrainingJobStatus({ jobId, status, timeStep = 5000, attemps = 0, callback, reqId = null }) {
        if(jobId == null) return callback("bad parameters [jobId] in waitTrainingJobStatus", null);
        if(status == null) return callback("bad parameters [status] in waitTrainingJobStatus", null);
        
        if(this[_log] != null) this[_log].info({ message: `Checking the job [${jobId}] status...`, domain: "azure-ml-manager", reqId });
        this[_doWaitTask]({ jobId, status, timeStep, attemps, reqId, callback });
    }    

    /** ENTRENAMIENTO **/    
    /**
     * Realiza todo el flujo necesario para reentrenar un modelo de Azure ML: crea la tarea, la inicia y espera a que esté en estado Finished (finalizado)
     * @param {string} containerName Nombre del contenedor donde se almacenarán los resultados del reentrenamiento
     * @param {Object[]} inputs Array de elementos de entrada para la tarea de reentrenamiento
     * @param {string} input.model Nombre del input dentro del experimento de Azure
     * @param {string} input.blobName Nombre del blob en Azure Storage
     * @param {Object[]} outputs Array de elementos de salida de la tarea de reentrenamiento
     * @param {string} output.model Nombre del output dentro del experimento de Azure
     * @param {string} output.blobName Nombre del blob en Azure Storage, donde se almacenará la salida
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {string} jobId Identificador de la tarea de entrenamiento creada
     */
    retrainModel({ containerName, inputs, outputs, callback, reqId = null }) {
        if(containerName == null) return callback("bad parameters [containerName] in retrainModel", null);
        if(inputs == null) return callback("bad parameters [inputs] in retrainModel", null);
        if(outputs == null) return callback("bad parameters [outputs] in retrainModel", null);
        if(this[_storageManager] == null) return callback("storage manager missing", null);
        if(this[_sdk] == null) return callback("azureMLSDK missing", null);

        this[_doTrain]({ containerName, inputs, outputs, reqId, callback });
    }

    /**
     * Realiza todo el flujo necesario para reentrenar un modelo de Azure ML: crea la tarea, la inicia y espera a que esté en estado Finished (finalizado) 
     * y parchea (actualiza) todos los servicios webs especificados
     * @param {string} containerName Nombre del contenedor donde se almacenarán los resultados del reentrenamiento
     * @param {Object[]} inputs Array de elementos de entrada para la tarea de reentrenamiento
     * @param {string} input.model Nombre del input dentro del experimento de Azure
     * @param {string} input.blobName Nombre del blob en Azure Storage
     * @param {Object[]} outputs Array de elementos de salida de la tarea de reentrenamiento
     * @param {string} output.model Nombre del output dentro del experimento de Azure
     * @param {string} output.blobName Nombre del blob en Azure Storage, donde se almacenará la salida
     * @param {Object[]} patchList Objetos con la información de los modelos a parchear
     * @param {Object} patchList.sdk Instancia del SDK de Azure ML
     * @param {string} patchList.outputName Nombre del componente de salida del servicio web del modelo reentrenado
     * @param {string} patchList.resourceName Nombre del modelo reentrenado durante su exportación
     * @param {number} [timeStep=5000] Incremento de tiempo entre cada consulta por el estado de la tarea
     * @param {number} [attemps=0] Número máximo de consultas
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {string} jobId Identificador de la tarea de entrenamiento creada
     */
    retrainModelAndPatch({ containerName, inputs, outputs, patchList, timeStep = 5000, attemps = 0, callback, reqId = null }) {
        if(containerName == null) return callback("bad parameters [containerName] in retrainModelAndPatch", null);
        if(inputs == null) return callback("bad parameters [inputs] in retrainModelAndPatch", null);
        if(outputs == null) return callback("bad parameters [outputs] in retrainModelAndPatch", null);
        if(patchList == null) return callback("bad parameters [patchList] in retrainModelAndPatch", null);
        if(this[_storageManager] == null) return callback("storage manager missing", null);
        if(this[_sdk] == null) return callback("azureMLSDK missing", null);

        const doPatch = ({ jobId, callback }) => {
            Async.waterfall(
                [
                    callback => {
                        const statusFinished = TrainingJobStatus.Finished;
                        this.waitTrainingJobStatus({ jobId, status: statusFinished, timeStep, attemps, reqId, callback });
                    },
                    (results, callback) => {
                        if(results == null && results.Results == null) return callback("There was an error during retraining", null);
                        const trainResults = results.Results;
                        this.patchModelWithJobId({ patchList, jobId, reqId, callback });
                    }
                ],
                callback
            );
        };

        Async.waterfall(
            [
                callback => this.retrainModel({ containerName, inputs, outputs, reqId, callback }),                
                (jobId, callback) => {
                    if(jobId == null) return callback("There was an error during retraining", null);
                    callback(null, jobId);
                    doPatch({ 
                        jobId: jobId, 
                        callback: (err, results) => {
                            if(err != null) {
                                if(this[_log] != null) this[_log].error({ message: "There was an error during patching", domain: "azure-ml-manager", reqId, payload: err });
                            }
                        }
                    });
                },
            ],
            callback
        );
    }    
    
    /**
     * Realiza todo el flujo necesario, de forma síncrona, para reentrenar un modelo de Azure ML: crea la tarea, la inicia y espera a que esté en estado Finished (finalizado)
     * @param {string} containerName Nombre del contenedor donde se almacenarán los resultados del reentrenamiento
     * @param {Object[]} inputs Array de elementos de entrada para la tarea de reentrenamiento
     * @param {string} input.model Nombre del input dentro del experimento de Azure
     * @param {string} input.blobName Nombre del blob en Azure Storage
     * @param {Object[]} outputs Array de elementos de salida de la tarea de reentrenamiento
     * @param {string} output.model Nombre del output dentro del experimento de Azure
     * @param {string} output.blobName Nombre del blob en Azure Storage, donde se almacenará la salida
     * @param {number} [timeStep=5000] Incremento de tiempo entre cada consulta por el estado de la tarea
     * @param {number} [attemps=0] Número máximo de consultas
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Resultados de la tarea de reentrenamiento
     * @param {Date} result.timestamp Fecha en la que ha terminado la tarea de reentrenamiento
     * @param {string} result.jobId Identificador de la tarea de reentrenamiento
     * @param {Object} result.results Resultados de la tarea de reentrenamiento
     */
    retrainModelSync({ containerName, inputs, outputs, timeStep = 5000, attemps = 0, callback, reqId = null }) {
        if(containerName == null) return callback("bad parameters [containerName] in retrainModel", null);
        if(inputs == null) return callback("bad parameters [inputs] in retrainModel", null);
        if(outputs == null) return callback("bad parameters [outputs] in retrainModel", null);
        if(this[_storageManager] == null) return callback("storage manager missing", null);
        if(this[_sdk] == null) return callback("azureMLSDK missing", null);

        let jobId;
        Async.waterfall(
            [
                callback => this[_doTrain]({ containerName, inputs, outputs, timeStep, attemps, reqId, callback }),
                (result, callback) => { 
                    jobId = result;
                    const statusFinished = TrainingJobStatus.Finished;
                    this.waitTrainingJobStatus({ jobId, status: statusFinished, timeStep, attemps, reqId, callback });
                }, 
            ],
            (err, result) => {
                let _result = null;
                if(err == null) {
                    _result = {};
                    const { training } = result;
                    _result.timestamp = training.EndTime;
                    _result.jobId = jobId;
                    _result.results = training.Results;
                }
                callback(err, _result);
            }
        );
    }

    /**
     * Realiza todo el flujo necesario para reentrenar un modelo de Azure ML: crea la tarea, la inicia y espera a que esté en estado Finished (finalizado) 
     * y parchea (actualiza) todos los servicios webs especificados
     * @param {string} containerName Nombre del contenedor donde se almacenarán los resultados del reentrenamiento
     * @param {Object[]} inputs Array de elementos de entrada para la tarea de reentrenamiento
     * @param {string} input.model Nombre del input dentro del experimento de Azure
     * @param {string} input.blobName Nombre del blob en Azure Storage
     * @param {Object[]} outputs Array de elementos de salida de la tarea de reentrenamiento
     * @param {string} output.model Nombre del output dentro del experimento de Azure
     * @param {string} output.blobName Nombre del blob en Azure Storage, donde se almacenará la salida
     * @param {Object[]} patchList Objetos con la información de los modelos a parchear
     * @param {Object} patchList.sdk Instancia del SDK de Azure ML
     * @param {string} patchList.outputName Nombre del componente de salida del servicio web del modelo reentrenado
     * @param {string} patchList.resourceName Nombre del modelo reentrenado durante su exportación
     * @param {number} [timeStep=5000] Incremento de tiempo entre cada consulta por el estado de la tarea
     * @param {number} [timeStep=5000] Incremento de tiempo entre cada consulta por el estado de la tarea
     * @param {number} [attemps=0] Número máximo de consultas
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object[]} results Resultados de la tarea de reentrenamiento y parcheo
     */
    retrainModelAndPatchSync({ containerName, inputs, outputs, patchList, timeStep = 5000, attemps = 0, callback, reqId = null }) {
        if(containerName == null) return callback("bad parameters [containerName] in retrainModelAndPatch", null);
        if(inputs == null) return callback("bad parameters [inputs] in retrainModelAndPatch", null);
        if(outputs == null) return callback("bad parameters [outputs] in retrainModelAndPatch", null);
        if(patchList == null) return callback("bad parameters [patchList] in retrainModelAndPatch", null);
        if(this[_storageManager] == null) return callback("storage manager missing", null);
        if(this[_sdk] == null) return callback("azureMLSDK missing", null);
        
        Async.waterfall(
            [
                callback => this.retrainModelSync({ containerName, inputs, outputs, timeStep, attemps, reqId, callback }),
                (results, callback) => {
                    if(results == null && results.results == null) return callback("There was an error during retraining", null);
                    const trainResults = results.results;
                    const { jobId } = results;
                    this.patchModelWithJobId({ patchList, jobId, reqId, callback });
                }
            ],
            callback
        );
    }    

    /** PARCHEO */    
    /**
     * Consulta el estado de una tarea de reentrenamiento finalizada y parchea (actualiza) todos los servicios webs especificados
     * @param {Object[]} patchList Objetos con la información de los modelos a parchear
     * @param {Object} patchList.sdk Instancia del SDK de Azure ML
     * @param {string} patchList.outputName Nombre del componente de salida del servicio web del modelo reentrenado
     * @param {string} patchList.resourceName Nombre del modelo reentrenado durante su exportación
     * @param {string} jobId Identificador de la tarea a consultar
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object[]} results Resultados de la tarea de reentrenamiento y parcheo
     */
    patchModelWithJobId({ patchList, jobId, callback, reqId = null }) {                
        if(patchList == null) return callback("bad parameters [patchList] in patchModelWithJobId", null);
        if(jobId == null) return callback("bad parameters [jobId] in patchModelWithJobId", null);        
        if(this[_sdk] == null) return callback("azureMLSDK missing", null);

        const results = [];
        Async.series(
            [
                callback => this[_updatePatchJobStatus]({ jobId, patchStatus: PatchJobStatus.Running, reqId, callback }),
                callback => {
                    Async.waterfall(
                        [
                            callback => {
                                this[_sdk].getJob({ jobId, reqId, callback: (err, result) => {
                                    results.push(result);
                                    callback(null, result);
                                }});
                            },
                            (response, callback) => {
                                const statusFinished = TrainingJobStatus.Finished;
                                if(response.StatusCode !== statusFinished) return callback(`job [${jobId}] is not Finished`, null);
            
                                const trainResults = response.Results;
                                this[_patchModelWithResults]({ patchList, trainResults, reqId, callback });
                            },
                        ],
                        (err, result) => {
                            results.push(result);
                            callback(err, results);
                        }
                    );
                }
            ],
            (err, results) => {
                let result = null;
                if(err != null) {
                    this[_updatePatchJobStatus]({ 
                        jobId, 
                        patchStatus: PatchJobStatus.Failed,
                        endTime: new Date(),
                        detail: err,
                        reqId,
                        callback: (updateErr, updateResults) => {
                            callback(err, result);
                        }
                    });
                } else {
                    if(results != null && Array.isArray(results) && results.length >= 2) result = results[1];
                    this[_updatePatchJobStatus]({ 
                        jobId, 
                        patchStatus: PatchJobStatus.Finished, 
                        endTime: new Date(),
                        reqId,
                        callback: (updateErr, updateResults) => {
                            callback(err, result);
                        }
                    });
                }
            }
        );            
    }
}

module.exports = MLTrainBESManager;
module.exports.TrainingJobStatus = TrainingJobStatus;