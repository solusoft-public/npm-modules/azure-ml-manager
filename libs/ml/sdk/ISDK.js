const ISDK = class ISDK {

    /**
     * SDK de Azure ML
     * @param {Object} webService Objeto que define el servicio web del componente     
     * @param {string} webService.executeEndpoint Url de ejecución del servicio
     * @param {string} [webService.patchEndpoint] Url de ejecución del servicio
     * @param {string} webService.apiKey Clave de seguridad de la API
     * @param {string} webService.apiVersion Número de versión de la API     
     * @param {Object} [log] Objeto para el logeo de trazas
     */
    constructor({ webService, log = null }) {
        this._webService = webService;
        this._log = log || global.log;
    }

    /**
     * Crea, pero no inicia, la tarea de reentramiento de un modelo en Azure ML
     * @param {Object[]} inputs Array de elementos de entrada para la tarea de reentrenamiento
     * @param {string} input.model Nombre del input dentro del experimento de Azure
     * @param {string} input.blobName Nombre del blob en Azure Storage
     * @param {Object[]} outputs Array de elementos de salida de la tarea de reentrenamiento
     * @param {string} output.model Nombre del output dentro del experimento de Azure
     * @param {string} output.blobName Nombre del blob en Azure Storage, donde se almacenará la salida
     * @param {Object} globalParameters Parámetros de la llamada REST
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {string} result Id de la tarea
     */
    createJob({ inputs, outputs, globalParameters = {}, callback, reqId = null }) {
        throw new Error('Not yet implemented');
    }

    /**
     * Inicia la tarea de reentramiento de un modelo en Azure ML, a partir de su Id
     * @param {string} jobId Identificador de la tarea a iniciar
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result
     */
    startJob({ jobId, callback, reqId = null }) {
        throw new Error('Not yet implemented');
    }

    /**
     * Cancela una tarea de Azure ML, a partir de su Id
     * @param {string} jobId Identificador de la tarea a cancelar
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result
     */
    cancelJob({ jobId, callback, reqId = null }) {
        throw new Error('Not yet implemented');
    }

    /**
     * Consulta el estado o los resultados de una tarea
     * @param {string} jobId Identificador de la tarea a consultar
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Objeto que indica el estado de la tarea
     * @param {string} result.StatusCode Indicador del estado de la tarea [NotStarted, Running, Failed, Cancelled, Finished]
     * @param {Object} result.Results Objeto con los resultados, si los hay, de la tarea
     * @param {Object} result.Details Objeto de detalle
     */
    getJob({ jobId, callback, reqId = null }) {
        throw new Error('Not yet implemented');
    }

    /**
     * Parchea el modelo especificado del servicio web de Azure ML
     * @param {string} resourceName Nombre del recurso a parchear
     * @param {Object} blobFile Objeto con la información del blob en Azure Storage para parchear el servicio web
     * @param {string} blobFile.BaseLocation Url base del Azure Storage
     * @param {string} blobFile.RelativeLocation Ruta relativa del blob en Azure Storage
     * @param {string} blobFile.SasBlobToken Firma de acceso compartido (SAS) del blob en Azure Storage
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result
     */
    patchModel({ resourceName, blobFile, callback, reqId = null }) {
        throw new Error('Not yet implemented');       
    }

    /**
     * Realiza una predicción
     * @param {string} inputName Nombre del componente de entrada del servicio web
     * @param {Object} inputs Objeto con las entradas para realizar la predicción 
     * @param {Object} globalParameters Parámetros de la llamada REST
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Object} result Objeto que indica el resultado de la predicción
     */
    postPredictionRequest({ inputName, inputs, globalParameters = {}, callback, reqId = null }) {
        throw new Error('Not yet implemented');
    }
}

module.exports = ISDK;