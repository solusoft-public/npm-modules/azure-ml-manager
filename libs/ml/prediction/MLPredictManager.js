const Async = require('async');
const clone = require('clone');

//properties
const _components = Symbol("components");
const _ensembles = Symbol("ensembles");
const _log = Symbol("log");

const MLPredictManager = class MLPredictManager {

    /**
     * Clase encargada de gestionar el flujo necesario para realizar una predicción de un conjunto de características 
     * @param {Array.<AzureDataTable|AzureMLWebService|IComponent>} [components] Vector de componentes a utilizar para realizar la predicción     
     * @param {Array.<AzureMLWebService|IEnsemble>} [ensembles] Vector de componentes a utilizar para realizar la desambiguación de vectores de salida
     * de componentes
     * @param {Object} [log] Objeto para el logeo de trazas
     */
    constructor({ components = null, ensembles = null, log = null }) {
        this[_components] = components;
        this[_ensembles] = ensembles;
        this[_log] = log || global.log;
    }

    /**
     * @typedef AzureDataTable Componente que consulta una tabla de Azure Table Storage
     * @type {Object}
     * @property {string} name Nombre del componente
     * @property {string[]} outputColumns Vector de columnas que especifica las columnas de la salida del compomente
     * @property {Object} storageManager Instancia del módulo encargado de gestionar las operaciones sobre Azure Storage
     * @property {string} tableName Nombre de la tabla de Azure
     * @property {Object} query Objeto Azure.TableQuery a utilizar para consultar la tabla de Azure
     * @property {Object} [log] Objeto para el logeo de trazas
     * 
     * @typedef AzureMLWebService Componente que invoca un servicio web de Azure ML
     * @type {Object}
     * @property {string} name Nombre del componente
     * @property {string[]} outputColumns Vector de columnas que especifica las columnas de la salida del compomente
     * @property {string} inputName Nombre del componente de entrada del servicio web
     * @property {string} outputName Nombre del componente de salida del servicio web
     * @property {string[]} inputColumns Vector de columnas que especifica las columnas de la salida del compomente     
     * @property {Object} azureMLSDK Instancia de AzureMLSdk
     * @property {Object} [log] Objeto para el logeo de trazas
     * 
     * @typedef IComponent - Interfaz para definir un componente
     * @type {Object}
     * @property {string} name Nombre del componente
     * @property {string[]} outputColumns Vector de columnas que especifica las columnas de la salida del compomente
     * @property {Object} [log] Objeto para el logeo de trazas
     * 
     * @typedef IEnsemble - Interfaz para definir un objeto para la desambiguación
     * @type {Object}
     * @property {string} name Nombre del componente
     * @property {string[]} outputColumns Vector de columnas que especifica las columnas de la salida del compomente
     * @property {Object} [log] Objeto para el logeo de trazas
     */

    /**
     * Obtiene el array de vectores de salida de cada componente especificado
     * @param {Array.<string|number>} input Vector de características de entrada para la predicción
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Array.<Array.<string|number>>} result Array de vectores de componentes con una posición por cada característica involucrada en 
     * la predicción. En caso de que se haya utilizado más de un componente para predecir una característica, habrá más de un vector en esa posición 
     * del array
     */
    getComponentsOutputs({ input, callback, reqId = null }) {
        if(this[_components] == null) return callback("components missing", null);

        Async.map(
            this[_components],
            (component, callback) => {
                component.getComponentOutput({ input, reqId, callback: (err, result) => {
                    let _result = result;
                    //Si hay un error o el resultado no tiene la forma que esperamos
                    if(err != null || (result == null || !(Array.isArray(result)))) {
                        if(err != null && this[_log] != null) {
                            let error = err;
                            if(err != null && err instanceof Error && err.stack != null) {
                                error = err.stack;
                            }
                            this[_log].error({ message: `Error in component ${component.getName()}`, domain: "azure-ml-manager", reqId, payload: error });
                        }
                        const emptyResult = new Array(component.getOutputColumns().length);
                        emptyResult.fill(null);
                        _result = emptyResult;
                    }
                    callback(null, _result);
                }});
            },
            (err, results) => {
                const arraysLengths = results.map(result => result.length);
                const maxArrayLength = Math.max.apply(Math, arraysLengths);
                const componentOutputs = new Array(maxArrayLength);
                componentOutputs.fill(null);
                
                for(let result of results) {
                   for(let i = 0; i < result.length; i++) {
                       if(result[i] != null) {
                           if(componentOutputs[i] == null) componentOutputs[i] = [];
                           componentOutputs[i].push(result);
                       }
                   } 
                }
                callback(err, componentOutputs);
            }
        );
    }

    /**
     * Disambigua el array de vectores de salida de componentes para aquellas características en las que se ha utilizado más de un componente
     * @param {Array.<Array.<string|number>>} componentOutputsVector Array de vectores de componentes con una posición por cada característica 
     * involucrada en la predicción. En caso de que se haya utilizado más de un componente para predecir una característica, habrá más de un vector 
     * en esa posición del array
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Array.<Array.<string|number>>} result Array de vectores de componentes con una posición por cada característica involucrada en 
     * la predicción y un sólo vector de componentes por característica      
     */
    disambiguateVectors({ componentOutputsVector, callback, reqId = null }) {
        const reduceDimensionality = (matrix) => {
            let reducedVector = clone(matrix);
            reducedVector = Object.keys(reducedVector).map(key => {
                const vector = reducedVector[key];
                if(vector != null && vector.length == 1) {
                    return vector[0];
                } else {
                    return null;
                }
            });
            return reducedVector;
        };
        if(this[_ensembles] == null) return callback(null, reduceDimensionality(componentOutputsVector));        

        Async.mapValues(
            componentOutputsVector,
            (componentOutputs, index, callback) => {
                if(componentOutputs != null && componentOutputs.length > 1) {
                    const ensembleInputVector = componentOutputs.map(componentOutput => {
                        return componentOutput[index];
                    });
                    const ensemble = this[_ensembles][index];
                    if(ensemble == null) return callback(`Ensemble needed for index [${index}]`, null);
                    
                    ensemble.disambiguateVector({ input: ensembleInputVector, reqId, callback: (err, result) => {
                        let _result = result;
                        //Si hay un error o el resultado no tiene la forma que esperamos
                        if(err != null || (result == null || !(Array.isArray(result)))) {
                            if(err != null && this[_log] != null) {
                                let error = err;
                                if(err != null && err instanceof Error && err.stack != null) {
                                    error = err.stack;
                                }
                                this[_log].error({ message: `Error in ensemble ${ensemble.getName()}`, domain: "azure-ml-manager", reqId, payload: error });
                            }
                            const emptyResult = new Array(ensemble.getOutputColumns().length);
                            emptyResult.fill(null);
                            _result = emptyResult;
                        }
                        callback(null, _result);
                    } });
                }
                else {
                    callback(null, null);
                }                
            },
            (err, results) => {              
                let disambiguatedVector = reduceDimensionality(componentOutputsVector);
                for(let i = 0; i < Object.keys(results).length; i++) {
                    if(results[i] != null) {
                        disambiguatedVector[i] = results[i];
                    }
                } 
                callback(err, disambiguatedVector);
            }
        );  
    }

    /**
     * Convierte el array de vectores de salida de los componentes desambiguados en un sólo array con una posición por cada característica predicha
     * @param {Array.<Array.<string|number>>} disambiguatedVector Array de vectores de componentes con una posición por cada característica 
     * involucrada en la predicción y un sólo vector de componentes por característica
     * @param {string} [reqId] Id de correlación  
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Array.<string|number>} result Vector de salida con los resultados del flujo de predicción     
     */
    reduceVectors({ disambiguatedVector, callback, reqId = null }) {
        let error = null;
        let reducedVector = null;
        try {
            reducedVector = disambiguatedVector.map((vector, index) => {
                let result = null;
                if(vector != null) result = vector[index];
                return result;
            });
        }
        catch(err) { error = err; }
        callback(error, reducedVector);
    }

    /**
     * Realiza el flujo de predicción sobre el conjunto de características dado
     * @param {Array.<string|number>} input Vector de características de entrada para la predicción
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Array.<string|number>} result Vector de salida con los resultados del flujo de predicción     
     */
    doPredictionFlow({ input, callback, reqId = null }) {    
        if(input == null) return callback("bad parameters [input] in doPredictionFlow", null);        
        //Pasos:
        //1. Obtener los component outputs para cada feature. Salida: diccionario <feature, array components outputs>
        //2. Desambiguar las features con más de un componente. Salida: diccionario <feature, component output>
        //3. Reducir el diccionario a un sólo EnsembleOutput

        Async.waterfall(
            [
                callback => this.getComponentsOutputs({ input, reqId, callback }),
                (componentOutputsVector, callback) => this.disambiguateVectors({ componentOutputsVector, reqId, callback }),
                (disambiguatedVector, callback) => this.reduceVectors({ disambiguatedVector, reqId, callback }),
            ],
            callback
        );
    }

}

module.exports = MLPredictManager;