const Async = require('async');
const AzureStorage = require('azure-storage');
const IComponent = require('../../IComponent');
const IStorage = require('../../../../../storage/IStorage');
const TableQuery = AzureStorage.TableQuery;

//properties
const _storageManager = Symbol("storageManager");
const _tableName = Symbol("tableName");
const _query = Symbol("query");


const AzureDataTable = class AzureDataTable extends IComponent {

    /**
     * Componente que consulta una tabla de Azure Table Storage
     * @param {string} name Nombre del componente
     * @param {string[]} outputColumns Vector de columnas que especifica las columnas de la salida del compomente
     * @param {Object} storageManager Instancia del módulo encargado de gestionar las operaciones sobre Azure Storage
     * @param {string} tableName Nombre de la tabla de Azure
     * @param {Object} query Objeto Azure.TableQuery a utilizar para consultar la tabla de Azure
     * @param {Object} [log] Objeto para el logeo de trazas
     */
    constructor({ name, outputColumns, storageManager, tableName, query, log = null }) {
        super({ name: name, outputColumns: outputColumns, log: log });
        this[_storageManager] = storageManager;
        this[_tableName] = tableName;
        this[_query] = query;
    }

    /**
     * Obtiene el vector de salida extraído de la tabla de Azure Table Storage
     * @param {Array.<string|number>} input Vector con las entradas para realizar la predicción 
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Array.<string|number>} result Vector de salida del componente con un valor nulo en todas las posiciones excepto en las de las 
     * características predichas
     */
    getComponentOutput({ input, callback, reqId = null }) {        
        if(this._outputColumns == null || !(Array.isArray(this._outputColumns))) {
            return callback(`outputColumns missing or wrong in component [${this._name}]`, null);
        }
        if(this[_storageManager] == null || !(this[_storageManager] instanceof IStorage)) {
            return callback(`storageManager missing or wrong in component [${this._name}]`, null);
        }
        if(this[_tableName] == null || !(typeof this[_tableName] === "string")) {
            return callback(`tableName missing or wrong in component [${this._name}]`, null);
        }
        if(this[_query] == null || !(this[_query] instanceof TableQuery)) {
            return callback(`query missing or wrong in component [${this._name}]`, null);
        }

        Async.waterfall(
            [
                callback => {                                        
                    this[_storageManager].queryTableEntities({ tableName: this[_tableName], query: this[_query], reqId, callback });
                },
                (entities, callback) => {
                    let error = null;
                    let result = null;
                    if(entities == null || !(Array.isArray(entities)) || entities.length == 0) return callback(`empty result in component [${this._name}]`, null);
                    try {                        
                        const [ entity ] = entities;                               
                        const vector = this._outputColumns.map(column => {
                            let value = null;
                            if(entity.hasOwnProperty(column)) {
                                value = entity[column]._ !== "" ? entity[column]._ : null;
                            }                         
                            return value;
                        });
                        result = vector;
                    } catch(err) {
                        error = err;
                    }
                    callback(error, result);
                },
            ],            
            callback
        );
    }

}

module.exports = AzureDataTable;