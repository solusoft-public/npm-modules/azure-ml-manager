const clone = require('clone');

const IEnsemble = class IEnsemble {

    /**
     * Interfaz para definir un componente
     * @param {string} [name] Nombre del componente
     * @param {string[]} outputColumns Vector de columnas que especifica las columnas de la salida del compomente
     * @param {Object} [log] Objeto para el logeo de trazas
     */
    constructor({ name = "Ensemble", outputColumns, log = null }) {
        this._name = name;
        this._outputColumns = outputColumns;
        this._log = log || global.log;
    }

    getName() {
        return this._name;
    }

    getOutputColumns() {
        return clone(this._outputColumns);
    }

    /**
     * Obtiene un vector desambiguado dado un vector de entrada
     * @param {Array.<string|number>} input Vector de características de entrada para la desambiguación. Este vector se construye con las predicciones 
     * realizadas por un conjunto de componentes sobre una característica
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Array.<string|number>} result Vector de salida del componente con un valor nulo en todas las posiciones excepto en las de las 
     * características predichas
     */
    disambiguateVector({ input, callback, reqId = null}) {
        throw new Error('Not yet implemented');
    }

}

module.exports = IEnsemble;