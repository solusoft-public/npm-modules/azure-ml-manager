const Async = require('async');
const Unirest = require('unirest');
const IEnsemble = require('../../IEnsemble');
const ObjectManager = require('../../../../../ObjectManager');
const ISDK = require('../../../../sdk/ISDK');

//properties
const _inputName = Symbol("inputName");
const _outputName = Symbol("outputName");
const _inputColumns = Symbol("inputColumns");
const _sdk = Symbol("sdk");


const AzureMLWebService = class AzureMLWebService extends IEnsemble {

    /**
     * Componente que invoca un servicio web de Azure ML
     * @param {string} name Nombre del componente
     * @param {string[]} outputColumns Vector de columnas que especifica las columnas de la salida del compomente
     * @param {string} inputName Nombre del componente de entrada del servicio web
     * @param {string} outputName Nombre del componente de salida del servicio web
     * @param {string[]} inputColumns Vector de columnas que especifica las columnas de la salida del compomente     
     * @param {Object} azureMLSDK Instancia de AzureMLSdk
     * @param {Object} [log] Objeto para el logeo de trazas
     */
    constructor({ name, outputColumns, inputName, outputName, inputColumns, azureMLSDK, log = null }) {
        super({ name: name, outputColumns: outputColumns, log: log });
        this[_inputName] = inputName;
        this[_outputName] = outputName;
        this[_inputColumns] = inputColumns;
        this[_sdk] = azureMLSDK;
    }    

    /**
     * Obtiene un vector desambiguado dado un vector de entrada
     * @param {Array.<string|number>} input Vector de características de entrada para la desambiguación. Este vector se construye con las predicciones 
     * realizadas por un conjunto de componentes sobre una característica
     * @param {string} [reqId] Id de correlación
     * @callback callback
     * @param {Object} err Objeto con la descripción de error, en caso de que ocurra
     * @param {Array.<string|number>} result Vector de salida del componente con un valor nulo en todas las posiciones excepto en las de las 
     * características predichas
     */
    disambiguateVector({ input, callback, reqId = null }) {        
        if(input == null || !(Array.isArray(input))) {
            return callback(`bad parameters [input] in getComponentOutput in component [${this._name}]`, null);
        }
        if(this._outputColumns == null || !(Array.isArray(this._outputColumns))) {
            return callback(`outputColumns missing or wrong in component [${this._name}]`, null);
        }
        if(this[_inputName] == null || !(typeof this[_inputName] === "string")) {
            return callback(`inputName missing or wrong in component [${this._name}]`, null);
        }
        if(this[_outputName] == null || !(typeof this[_outputName] === "string")) {
            return callback(`outputName missing or wrong in component [${this._name}]`, null);
        }
        if(this[_inputColumns] == null || !(Array.isArray(this[_inputColumns]))) {
            return callback(`inputColumns missing or wrong in component [${this._name}]`, null);
        }
        if(this[_sdk] == null || !(this[_sdk] instanceof ISDK)) {
            return callback(`sdk missing or wrong in component [${this._name}]`, null);
        }
        
        Async.waterfall(
            [
                callback => {
                    const objectManager = new ObjectManager({});
                    const _input = objectManager.buildObject({ keys: this[_inputColumns], values: input });
                    this[_sdk].postPredictionRequest({ inputName: this[_inputName], inputs: [_input], globalParameters: {}, reqId, callback });
                },
                (result, callback) => {
                    let error = null;
                    let _result = null;
                    const outputName = this[_outputName];
                    if(result == null || result.Results == null || result.Results[outputName] == null) return callback(`empty result in component [${this._name}]`, null);                    
                    const entities = result.Results[outputName];
                    if(entities == null) return callback(`Bad parameter [${outputName}]`, null);
                    try {
                        const [ entity ] = entities;
                        const vector = this._outputColumns.map(column => {
                            let value = null;
                            if(entity.hasOwnProperty(column)) {
                                value = entity[column] !== "" ? entity[column] : null;
                            }                         
                            return value;
                        });
                        _result = vector;
                    } catch(err) {
                        error = err;
                    }
                    callback(error, _result);
                }
            ],
            callback
        );
    }

}

module.exports = AzureMLWebService;