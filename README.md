Módulo azure-ml-manager
=========================

El módulo azure-ml-manager tiene el propósito de actuar como interfaz para ejecutar las operaciones básicas de la plataforma Azure Machine Learning. A continuación se describirá la plataforma, la forma de uso del módulo y las distintas operaciones que el módulo habilita sobre ella.

__NOTA: Actualmente el módulo está adaptado para usar servicios web clásicos de Azure ML, no teniendo información de si estos servicios van a ser deprecados en un futuro. En la carpeta [doc/upgrade](doc/upgrade/) se puede consultar la conversación con el equipo de soporte de Azure en la que se nos orienta sobre cómo realizar la actualización.__   

## 1. Azure Machine Learning

Azure Machine Learning o Azure ML permite, entre otras cosas, construir modelos predictivos utilizando la amplia galería de algoritmos de Inteligencia Artificial y de componentes que Azure pone a disposición de los usuarios. El objetivo de usar esta plataforma es el de entrenar estos modelos con datos de tal forma que cuando llega un nuevo conjunto de datos el sistema ya haya aprendido y sea capaz de realizar "predicciones", o generalizaciones sobre ellos.

La plataforma dispone de la herramienta Azure Machine Learning Studio que permite, de forma sencilla arrastrando y colocando componentes, construir lo que denominan experimentos, los cuales pueden contener uno o más modelos de aprendizaje automático. También incluye un módulo que permite importar datos a la plataforma mediante diversas fuentes: datasets propios en formato subidos a la plataforma, una URL de un documento, Azure SQL Databases, Azure Tables...Finalmente permite publicar servicios web para acceder remotamente a las operaciones de entrenar un modelo o realizar una predicción.

## 2. Antes de comenzar

### 2.1. Instalación

```bash
npm install https://gitlab.com/solusoft-idi/azure-ml-manager.git
```

## 3. Uso

El módulo dispone de submódulos encargados de gestionar las operaciones básicas sobre la plataforma de Azure Machine Learning. Estas operaciones se pueden dividir en tres bloques: reentrenamiento, parcheo y predicción. Los submódulos encargados de gestionar estas operaciones son los submódulos TrainManager, encargado del reentrenamiento y el parcheo y PredictManager, para gestionar las predicciones.

### 3.1. Reentrenamiento

El proceso de reentrenamiento consiste en volver a entrenar los modelos ya entrenados previamente, con nuevos datos. Azure Machine Learning dispone de más mecanismos para realizar esto, pero el que se ha utilizado en este módulo consiste en la configuración de un servicio web destinado a este propósito. Una vez acabado el reentrenamiento se realiza el proceso de parcheo, para que los servicios webs de predicción funcionen con el nuevo modelo reentrenado, proceso que se detallará posteriormente.

#### 3.1.1. Requisitos previos

##### Cuenta Azure Storage

El proceso de reentrenamiento requiere de la información de conexión a una cuenta de Azure Storage, la cual utilizará para almacenar los resultados de reentrenamiento.

##### Servicio web de reentrenamiento

Para el reentrenamiento debe diseñarse primero un experimento con Azure ML Studio. La siguiente imagen muestra un experimento de ejemplo válido para este propósito.

![Ejemplo de experimento de reentrenamiento](doc/images/retrainExample.PNG)

Este experimento deberá tener un componente de Web service output y opcionalmente otro de Web service input. El componente de Web service output se utiliza para que el servicio web produzca una salida. El componente de Web service input es opcional ya que pueden especificarse inputs al llamar al servicio web o, como en el ejemplo de la foto, no especificarlos y utilizar un módulo que se conecte a una fuente de datos y los utilice como entrada para realizar el reentrenamiento. Más adelante se detallará el formato que debe utilizarse al especificar estos inputs y outputs.

Una vez generado el experimento se deberá generar el servicio web de reentrenamiento. Para ello deberá ejecutarse previamente el experimento y a continuación, se deberá hacer click en la opción Deploy Web Service.

![Consumo de servicio web](doc/images/deployWebService.PNG)

Una vez generado el servicio web de reentrenamiento deberemos consultar la información de acceso al mismo. Para ello iremos a la sección Web Services, seleccionaremos el servicio web que se acaba de crear y pulsaremos en el botón New Web Services Experience (preview). A continuación, iremos a la sección Consume. De esta sección guardamos los campos Primary Key (que denominaremos apiKey), Batch Requests (que denominaremos executeEndpoint) y anotaremos el valor del parámetro api-versión. Los servicios web en Azure tienen dos modos de ejecución, el modo Request-Response y el modo Batch Request. En este caso utilizaremos el modo Batch Requests ya que permite que el servicio web produzca varias salidas.

![Deploy Web Service](doc/images/webServiceConsume.PNG)

Para el reentrenamiento invocaremos este servicio web y todas las salidas se almacenarán en la cuenta de Azure Storage configurada, más adelante observaremos un ejemplo.

##### Exportación de modelos

Posteriormente, para la generación de los servicios webs predictivos, será necesario exportar los modelos entrenados. Para ello se deberá hacer click derecho en el modelo entrenado y se deberá seleccionar la opción "Save as Trained Model". Se deberá anotar tanto el nombre que se le dé al modelo exportado como el nombre del componente Web service output que hayamos conectado al mismo. Más adelante se describirá el uso que se le da a estos modelos exportados.

![Exportación de modelos](doc/images/saveTrainedModel.PNG)

#### 3.1.2. Ejemplo

Para instanciar el módulo TrainManager se le debe proporcionar una instancia que gestione la cuenta de Azure Storage, donde se vayan a almacenar los resultados del reentrenamiento, y una instancia del SDK de Azure ML que se encargará de realizar las operaciones. Opcionalmente se puede incluir un objeto de Log para que se registren trazas, que implemente la interfaz que expone el módulo. Tanto para el gestor de Azure Storage como para el SDK de Azure ML el módulo incluye implementaciones y además expone interfaces para implementar los propios si se desea. El gestor de Azure Storage se instancia indicando el nombre de la cuenta de Azure Storage y el key. El SDK de Azure ML se instancia indicándole el endpoint de ejecución, el apiKey y el apiVersion, los cuales hemos obtenidos anteriormente. A continuación, se incluye un ejemplo de entrenamiento de un modelo.

```js
const AzureMlManager = require('azure-ml-manager');
const TrainManager = AzureMlManager.Managers.TrainManager;
const StorageManager = AzureMLManager.Storage.StorageManager;
const SDK = AzureMlManager.SDK.UnirestSDK;

const storageManager = new StorageManager({ accountName: "<accountName>", key: "<key>" });
const trainingWS = {
  "executeEndpoint": "<executeEndpoint>",
  "apiKey": "<apiKey>",
  "apiVersion": "<apiVersion>"
};
const trainingSDK = new SDK({ webService: trainingWS });
const trainManager = new TrainManager({ storageManager: storageManager, azureMLSDK: trainingSDK });
const inputs = []; //En este ejemplo no se incluyen inputs porque se está usando el módulo Import Data
const outputs = [
  {
    "model": "output1",
    "blobName": "output1results.ilearner"
  }
];
trainManager.retrainModel({
  containerName: "<containerName>",
  inputs: inputs,
  outputs: outputs,
  callback: (err, result) => {
    //err: Objeto con la descripción de error, en caso de que ocurra
    //result: Identificador de la tarea de reentrenamiento creada
  }
});
```

Como salida de la función retrainModel se obtiene un identificador de tarea. Se puede consultar el estado de la tarea, utilizando la función getJobStatus. Opcionalmente se puede utilizar la llamada síncrona retrainModelSync que espera a que acabe el reentrenamiento.

### 3.2. Parcheo

El parcheo de servicios web consiste en actualizar los servicios webs creados para realizar la predicción, de forma que funcionen con los nuevos modelos reentrenados. El gestor de reentrenamiento TrainManager dispone de métodos para realizar el parcheo a partir de un id de tarea de reentrenamiento o a partir de sus resultados, por lo que se pueden realizar las tareas de reentrenamiento y de parcheo de forma independiente. No obstante, la forma natural de hacer el proceso de parcheo es justo a continuación del proceso de reentrenamiento. Con este propósito se utiliza la llamada retrainModelAndPatch, del módulo TrainManager.

#### 3.2.1. Requisitos previos

##### Cuenta Azure Storage

Se utiliza un contenedor de Azure Storage para almacenar los resultados de la tarea de forma que posteriormente, con la llamada getJobStatus, el usuario pueda consultar si el proceso de parcheo se ha iniciado o no, se está ejecutando, ha terminado o ha fallado. Este contenedor intermedio es necesario ya que en Azure ML el proceso de reentrenamiento y el de parcheo son procesos separados y es necesario un contenedor intermedio que permita almacenar el estado del proceso completo.

##### Servicio web predictivo

Como ya se ha comentado, el proceso de parcheo consiste en actualizar los servicios webs de predicción. Para crear un servicio web de predicción se debe crear inicialmente un experimento en Azure ML Studio. Este experimento tendrá una forma como la de la siguiente imagen:

![Servicio web de predicción](doc/images/predictionWS.PNG)

En este servicio se incluye un componente de Web service input, que será el que permitirá introducir entradas al invocar al servicio web. Los componentes "Import Data" y "Select Columns in Dataset" se utilizan porque Azure ML necesita que el componente "Web service input" esté conectado a un componente que le indique el esquema de los datos de entrada, para realizar validaciones. Se podrían haber utilizado otros componentes (como por ejemplo el Enter Data Manually que permite construir manualmente datasets), ya que esto únicamente es necesario para que Web service input conozca el esquema de datos. El componente de nombre "readme trained model" es un modelo previamente entrenado y exportado, tal y como se comentó anteriormente en la sección Reentrenamiento. Estos componentes se pueden encontrar en Azure ML Studio, a la izquierda, en la lista de componentes, bajo la subsección "Trained Models". Finalmente, con el componente Score Model se realiza la predicción, con el componente "Select Columns in Dataset" se seleccionan sólo los cambios relevantes y el componente "Web service output" se utiliza para que el servicio web produzca la salida. Una vez ejecutado el componente, y al igual que se hizo con el experimento de reentrenamiento, se debe hacer click en la opción "DEPLOY WEB SERVICE".

Una vez generado este servicio web este se crea con un endpoint por defecto, que siempre utiliza el modelo con el que se creó para realizar las predicciones. Para permitir parchear el servicio web de forma que funcione con nuevos modelos entrenados se debe crear un nuevo endpoint para el servicio web. Para ello iremos a la sección Web Services, seleccionaremos el servicio web que se acaba de crear y pulsaremos en el botón New Web Services Experience (preview), igual que se hizo anteriormente. Esto automáticamente abre el endpoint por defecto por lo que debe navegarse hacia atrás pulsando el botón con la flecha hacia la izquierda, encima del nombre del endpoint (que será default). Accederemos a una sección similar a la de la siguiente imagen.

![Crear nuevo endpoint](doc/images/newEndpoint.PNG)

En esta sección, pulsando en "NEW", crearemos un nuevo endpoint, indicando un nombre para el mismo. Una vez creado aparecerá en la lista inferior, y lo seleccionaremos para poder consultar sus datos de acceso. En este caso debemos anotar los valores Primary Key (que llamaremos apiKey), Request-Response (que llamaremos executeEndpoint), Patch (que llamaremos patchEndpoint) y anotaremos el valor del parámetro api-version. Adicionalmente, para realizar el parcheo debemos conocer el nombre del modelo reentrenado y el nombre del componente Web service output, que anotamos al exportar el modelo durante la etapa de reentrenamiento (sección Exportación de modelos).

#### 3.2.2. Ejemplo

En este ejemplo se utiliza la llamada retrainModelAndPatch del módulo TrainManager. Para ello se debe construir una instancia del objeto TrainManager (tal y como se comentó anteriormente). Para invocar la llamada retrainModelAndPatch se le debe indicar un array de objetos, cada uno de ellos con la siguiente información del modelo a parchear:
- __sdk:__ instancia del SDK de Azure ML.
- __outputName:__ nombre del componente Web service output, anotado al exportar el modelo durante la etapa de reentrenamiento.
- __resourceName:__ nombre del modelo reentrenado, que se le dio durante la exportación del modelo.

Con esta información ya se puede utilizar la llamada retrainModelAndPatch. A continuación, se incluye un ejemplo de uso:

```js
const AzureMlManager = require('azure-ml-manager');
const TrainManager = AzureMlManager.Managers.TrainManager;
const StorageManager = AzureMLManager.Storage.StorageManager;
const SDK = AzureMlManager.SDK.UnirestSDK;

const storageManager = new StorageManager({ accountName: "<accountName>", key: "<key>" });
const trainingWS = {
  "executeEndpoint": "<executeEndpoint>",
  "apiKey": "<apiKey>",
  "apiVersion": "<apiVersion>"
};
const trainingSDK = new SDK({ webService: trainingWS });
const trainManager = new TrainManager({ storageManager: storageManager, azureMLSDK: trainingSDK });

const modelWS = {
  "executeEndpoint": "<executeEndpoint>",
  "patchEndpoint": "<patchEndpoint>",
  "apiKey": "<apiKey>",
  "apiVersion": "<apiVersion>"
};
const modelSDK = new SDK({ webService: modelWS });
const patchList = [
  {
    "sdk": modelWS,
    "outputName": "<outputName>",
    "resourceName": "<resourceName>"
  }
];
const inputs = []; //En el ejemplo no se incluyen inputs porque se está usando el módulo Import Data
const outputs = [
  {
    "model": "output1",
    "blobName": "output1results.ilearner"
  }
];
trainManager.retrainModelAndPatch({
  containerName: "<containerName>",
  inputs: inputs,
  outputs: outputs,
  patchList: patchList,
  callback: (err, result) => {
    //err: Objeto con la descripción de error, en caso de que ocurra
    //result: Identificador de la tarea de reentrenamiento creada
  }
});
```

Como salida de la función retrainModelAndPatch se obtiene un identificador de tarea. El módulo ejecutará el parcheo automáticamente al acabar el reentrenamiento. Se puede consultar el estado de la tarea, utilizando la función getJobStatus. Opcionalmente se puede utilizar la llamada síncrona retrainModelAndPatchSync que espera a que acabe el reentrenamiento y el parcheo, devolviendo los resultados.

### 3.3. Predicción

El proceso de predicción consiste en, a partir de un vector de entrada, ejecutar distintos componentes para obtener un vector de salida con todas las predicciones. El submódulo encargado de realizar este proceso es el PredictManager. Este módulo contiene componentes para invocar un servicio web predictivo de Azure ML y consultar una tabla de Azure Tables. Además, también habilita una capa de "ensembles" que permiten desambiguar vectores en el caso de que se haya aplicado la técnica de Stacking. Esta técnica, de manera general, consiste en combinar clasificadores generados a partir de distintos algoritmos de aprendizaje con la finalidad de aprovechar las heurísticas de cada algoritmo Con las predicciones de los algoritmos de aprendizaje base se entrena un nuevo modelo de aprendizaje, que se suele denominar strong learner, desambiguando y produciendo una salida. El módulo contiene un ensemble con la capacidad de ejecutar un servicio web predictivo de Azure ML. El módulo expone además interfaces para que se implementen componentes y ensembles propios.

El flujo de predicción consiste en lo siguiente:
- __Obtención de salida de componentes:__ se ejecutan todos los componentes configurados y se procesa su salida. Se obtiene como resultado un array de arrays (matriz) con una posición por cada característica involucrada en la predicción. En cada posición del array habrá un vector de salida de componente, o más de uno en caso de que se haya configurado más de un componente para una característica (Stacking).
- __Desambiguación de vectores:__ se procesa la matriz obtenida de la etapa anterior y se desambiguan los casos en los que se haya utilizado más de un componente para una característica. En esta fase se ejecutan los ensembles configurados, encargados de resolver esa desambiguación. Como resultado de esta fase se obtiene un array de arrays con tantas posiciones como características involucradas en la predicción, y un sólo vector de salida en cada posición del array.
- __Reducción de vectores:__ se procesa la matriz obtenida de la etapa anterior y se convierte en un array. Por cada fila de la matriz obtenida se obtiene la columna correspondiente, construyendo así el array final.

Cada etapa del flujo se puede ejecutar por separado, funciones getComponentsOutputs, disambiguateVectors y reduceVectors respectivamente, o se puede invocar a la función doPredictionFlow para ejecutar el flujo completo.

#### 3.3.1. Componentes

Todos los componentes comparten una interfaz común. El usuario puede crear sus propios componentes implementando esta interfaz (IComponent). La información mínima para instanciar un componente es:
- __Nombre__: identificación del componente.
- __Vector de salida__: vector con un valor null en cada posición excepto en la posición en la que se desee resultado. En esta posición se debe incluir el nombre del campo que el componente produce como salida. Por ejemplo, los servicios webs predictivos de Azure suelen producir como salida un campo "Scored Labels". Un vector de salida de ejemplo podría ser el siguiente: [null, null, "Scored Labels", null].

La lista de componentes implementados es la siguiente:

##### AzureDataTable

Componente que consulta una tabla de Azure Tables. La salida de este componente es el resultado de la query.

###### Requisitos previos

Este componente requiere tener configurada una cuenta de Azure Storage y conocer sus datos de acceso. Además, deben conocerse los campos identificativos con los que se quiere construir la query.

###### Instanciación

Para instanciar este componente se le debe incluir, además de los campos "name" y "outputColumns" propios de todos los componentes, una instancia del objeto gestor de Azure Storage, el nombre de la tabla a consultar y el objeto TableQuery con la consulta a realizar.

A continuación, se incluye un ejemplo de instanciación:

```js
const AzureMlManager = require('azure-ml-manager');
const AzureDataTable = AzureMlManager.Components.AzureDataTable;
const StorageManager = AzureMLManager.Storage.StorageManager;
const TableQuery = AzureMlManager.Storage.TableQuery;

const storageManager = new StorageManager({ accountName: "<accountName>", key: "<key>" });
const query = new TableQuery().where('PartitionKey eq ? and RowKey eq ?', "<pkey>", "<rkey>");
const component = new AzureDataTable({
  name: "C_IMG",
  outputColumns: ["sex"],
  query: query,
  storageManager: storageManager,
  tableName: "componentOutputs"
});
```

##### AzureMLWebService

Componente que consulta un servicio web de Azure ML. La salida de este componente es la salida del servicio web.

###### Requisitos previos

El servicio web predictivo debe construirse con una estructura específica, la cual ya se describió en la sección "Parcheo". Tal y como se comentó en esa sección, para cada servicio web predictivo, debemos conocer los valores Primary Key (apiKey), Request-Response (executeEndpoint) y el valor del parámetro api-version.

###### Instanciación

Para instanciar este componente se le debe incluir lo siguiente:
- __inputName__: nombre del campo de entrada del servicio web. Se corresponde con el nombre del componente "Web service input", del experimento de Azure ML Studio que representa este servicio web.
- __outputName__: nombre del campo de salida del servicio web. Se corresponde con el nombre del componente "Web service output", del experimento de Azure ML Studio que representa este servicio web.
- __inputColumns__: vector con los nombres de las características que se van a enviar al servicio web. Por ejemplo, si el vector que se va a predecir tiene un tamaño de 15, pero sólo se le quieren mandar al servicio un subconjunto de las características, se construye un vector de la siguiente forma: ["age", "workClass", "fnlwgt", "education", "race", "income"].
- __azureMLSDK__: instancia del SDK de Azure ML

A continuación, se incluye un ejemplo de instanciación:

```js
const AzureMlManager = require('azure-ml-manager');
const AzureMLWebService = AzureMlManager.Components.AzureMLWebService;
const SDK = AzureMlManager.SDK.UnirestSDK;

const modelWS = {
  "executeEndpoint": "<executeEndpoint>",
  "apiKey": "<apiKey>",
  "apiVersion": "<apiVersion>"
};
const modelSDK = new SDK({ webService: modelWS });
const component = new AzureMLWebService({
  name: "C_IMG",
  outputColumns: ["Scored Labels"],
  inputName: "input1",
  outputName: "output1",
  inputColumns: ["age", "workClass", "fnlwgt", "education", "maritalStatus", "race", "income"],
  azureMLSDK: modelSDK
});
```

#### 3.3.2. Ensembles

Todos los ensembles comparten una interfaz común. El usuario puede crear sus propios componentes implementando esta interfaz (IEnsemble). La información mínima para instanciar un ensemble es la misma que para instanciar un componente.

La lista de ensembles implementados es la siguiente:

##### AzureMLWebService

Ensemble que consulta un servicio web de Azure ML. La salida de este ensemble es la salida del servicio web.

###### Instanciación

La forma de instanciar este ensemble es exactamente la misma que su componente homónimo.

#### 3.3.3. Ejemplo

Para instanciar el submódulo PredictManager se le proporciona una lista de componentes a utilizar y una lista de ensembles. Opcionalmente se puede incluir un objeto de Log para que se registren trazas, que implemente la interfaz que expone el módulo. Los componentes y los ensembles deben instanciarse siguiendo lo comentado anteriormente. Una vez instanciado el submódulo PredictManager se invoca a la función doPredictionFlow, indicando el vector de características que se utilizará para la predicción.

A continuación, se incluye un ejemplo de uso de un flujo de predicción con etapa de desambiguación. En este ejemplo se ha configurado un flujo de predicción que incluye una conexión con una tabla de Azure que tiene almacenada las salidas de un componente y una conexión con un servicio web de Azure ML. Ambos componentes producen una predicción sobre la misma característica, "sex" en este caso, por lo que es necesario desambiguar las salidas de los componentes. Para ello se configura un ensemble que consulta un servicio web predictivo con un modelo de aprendizaje (strong learner), que utiliza como entradas para la predicción las salidas de las predicciones realizadas por los dos componentes.

```js
const AzureMlManager = require('azure-ml-manager');
const AzureDataTable = AzureMlManager.Components.AzureDataTable;
const AzureMLWebService = AzureMlManager.Components.AzureMLWebService;
const AzureMLWebServiceEnsemble = AzureMlManager.Ensembles.AzureMLWebServiceEnsemble;
const StorageManager = AzureMLManager.Storage.StorageManager;
const TableQuery = AzureMlManager.Storage.TableQuery;
const SDK = AzureMlManager.SDK.UnirestSDK;
const PredictManager = AzureMlManager.Managers.PredictManager;

const components = [];
const storageManager = new StorageManager({ accountName: "<accountName>", key: "<key>" });
const query = new TableQuery().where('PartitionKey eq ? and RowKey eq ?', "<pkey>", "<rkey>");
const ADTComponent = new AzureDataTable({
  name: "C_IMG",
  outputColumns: ["sex"],
  query: query,
  storageManager: storageManager,
  tableName: "componentOutputs"
});
const modelWS = {
  "executeEndpoint": "<executeEndpoint>",
  "apiKey": "<apiKey>",
  "apiVersion": "<apiVersion>"
};
const modelSDK = new SDK({ webService: modelWS });
const AWSComponent = new AzureMLWebService({
  name: "C_GEN_SVM",
  outputColumns: ["Scored Labels"],
  inputName: "input1",
  outputName: "output1",
  inputColumns: ["age", "workClass", "fnlwgt", "education", "maritalStatus", "race", "income"],
  azureMLSDK: modelSDK
});
components.push(ADTComponent);
components.push(AWSComponent);

const ensembles = [];
const ensembleWS = {
  "executeEndpoint": "<executeEndpoint>",
  "apiKey": "<apiKey>",
  "apiVersion": "<apiVersion>"
};
const ensembleSDK = new SDK({ webService: ensembleWS });
const AWSEnsemble = new AzureMLWebServiceEnsemble({
  name: "C_SEX_SL",
  outputColumns: ["Scored Labels"],
  inputName: "input1",
  outputName: "output1",
  inputColumns: ["label1", "label2"],
  azureMLSDK: ensembleSDK
});
ensembles.push(AWSEnsemble);

const input = [39, "State-gov", 77516, "Bachelors", "Never-married", "White", "<=50K"];
const predictManager = new PredictManager({ components: components, ensembles: ensembles });
predictManager.doPredictionFlow({
      input: input,
      callback: (err, result) => {
        //err: Objeto con la descripción de error, en caso de que ocurra
        //result: vector de salida con las predicciones
      }
    });
```

## 4. Contributing & License

Solusoft

Todos los derechos reservados. Consultar [www.solusoft.es](http://www.solusoft.es/contactar.aspx)
